package souls.undead.characters;

import souls.undead.inventory.Armor;

public class BlackKnight extends Neutral{
	public BlackKnight() {
		super("Black Knight", 25, 12, 23, 23, 0, 0);
		updateStats();
		addSprite(new String[]{"","","","npc_blackknight.png"});
		setDirection(3);
		setSouls(15);
		setRewardRarity(1);
		setReward(new Armor("Wolf Armor","icon_artoriasarmor.png","PDEF+4",1000,4,0));
	}

}
