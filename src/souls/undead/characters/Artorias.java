package souls.undead.characters;

import souls.undead.inventory.Weapon;

public class Artorias extends Neutral{
	public Artorias(){
		super("Artorias",30,15,40,20,1,1);
		updateStats();
		addSprite(new String[]{"","","","npc_artorias.png"});
		setDirection(3);
		setSouls(30);
		setRewardRarity(1);
		setReward(new Weapon("Abyss Greatsword","icon_abyssgreatsword.png","PATK+15",1000,15,0.9,0));
	}

}
