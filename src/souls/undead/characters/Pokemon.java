package souls.undead.characters;

import souls.undead.actions.FireBall;

public final class Pokemon extends Pc{
	//CONSTRUCTOR
	public Pokemon(){
		super("Pikachu","Pok�mon",2,10,1,15,20,5);
		updateStats();
		setRange(3);
		setElement(1);
		addSprite(new String[]{"pokemon_right.png","pokemon_left.png","pokemon_up.png","pokemon_down.png"});
		addAttackSprite2(new String[]{"pokemon_right.png","pokemon_left.png","pokemon_up.png","pokemon_down.png"});
		setEvasionSprite("");
		setSkill1(new FireBall());
	}
}
