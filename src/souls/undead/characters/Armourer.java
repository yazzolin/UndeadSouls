package souls.undead.characters;

import java.util.ArrayList;

import souls.undead.inventory.Armor;
import souls.undead.inventory.Item;

public class Armourer extends Neutral{
	//ATTRIBUTES

	
	//CONSTRUCTOR
	public Armourer(){
		super("Maughlin",2,2,2,2,2,2);
		updateStats();
		addSprite(new String[]{"","","","npc_armourer.png"});
		setShopSprite("shop_armourer.png");
		setDirection(3);
		setSouls(2);
		initShop();
	}
//=====GETTERS=&=SETTERS=====//
	public ArrayList<Item> getShop() {
		return shop;
	}
	public void initShop(){
		shop.add(new Armor("Elite Knight","icon_eliteknightarmor.png","PDEF+2",50,2,0));
		shop.add(new Armor("Shogun","icon_alonnearmor.png","PDEF+6",75,6,0));
		shop.add(new Armor("Havel","icon_havelarmor.png","PDEF+10",200,10,0));
		shop.add(new Armor("Magic cloak","icon_magecloak.png","MDEF+2",50,0,2));
		shop.add(new Armor("Herald's cloak","icon_magecloak.png","MDEF+4",75,0,4));
		shop.add(new Armor("Archmage veil","icon_mageveil.png","MDEF+6",100,0,6));
		shop.add(new Armor("Armor of Favor","icon_favorarmor.png","PDEF+1 MDEF+1",60,1,1));
		shop.add(new Armor("Alonne Knight","icon_alonneknightarmor.png","PDEF+3 MDEF+3",120,3,3));
	}
	
} // end of class