package souls.undead.characters;

import souls.undead.inventory.Consumable;
import souls.undead.inventory.Item;
import souls.undead.utility.Dice;

public final class Ghost extends Enemy{
	//CONSTRUCTOR
	public Ghost(){		
		super("Ghost",-1,4,0,-1,5,2);
		updateStats();
		addSprite(new String[]{"ghost.png","ghost.png","ghost.png","ghost.png"});
		//addAttackSprite1(new String[]{"ghost.png","ghost.png","ghost.png","ghost.png"});
		addAttackSprite2(new String[]{"ghost.png","ghost.png","ghost.png","ghost.png"});
		setDirection(Dice.roll(0, 3));
		setSouls(2);
		setElement(1);
		Item reward = null;
		int r = Dice.roll(0, 2);
		switch(r){
		case 0:
			reward = new Consumable("Health Potion","icon_hpot.png","",10,1,10,0);
			break;
		case 1:
			reward = new Consumable("Mana Potion","icon_mpot.png","",10,1,20,0);
			break;
		case 2:
			reward = new Consumable("Stamina Potion","icon_spot.png","",10,1,5,0);
			break;
		}
		setReward(reward);
	}
}
