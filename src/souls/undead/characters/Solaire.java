package souls.undead.characters;

import souls.undead.utility.Dice;

public final class Solaire extends Enemy {
	//CONSTRUCTOR
	public Solaire() {
		super("Solaire", 30, 20, 15, 10, 10, 10);
		updateStats();
		addSprite(new String[]{"solaire_right.png","solaire_left.png","solaire_up.png","solaire_down.png"});
		//addAttackSprite1(new String[]{"solaire_right.png","solaire_left.png","solaire_up.png","solaire_down.png"});
		addAttackSprite2(new String[]{"solaire_praise.png","solaire_praise.png","solaire_praise.png","solaire_praise.png"});
		setDirection(Dice.roll(0, 3));
		setAggroRange(20);
	}

}
