package souls.undead.characters;

import souls.undead.actions.LeapAttack;
import souls.undead.actions.Whirlwind;
import souls.undead.inventory.Weapon;

public final class Warrior extends Pc{
	//CONSTRUCTOR
	public Warrior(){
		super("Player","Warrior",5,3,5,3,1,1);
		addSprite(new String[]{"warrior_right.png","warrior_left.png","warrior_up.png","warrior_down.png"});
		//addAttackSprite1(new String[]{"warrior_atkright1.png","warrior_atkleft1.png","warrior_atkup1.png","warrior_atkdown1.png"});
		addAttackSprite2(new String[]{"warrior_atkright2.png","warrior_atkleft2.png","warrior_atkup2.png","warrior_atkdown2.png"});
		setEvasionSprite("warrior_evasion.png");
		getInventory().getEquipment().set(0, new Weapon("Wooden Sword","icon_bokutou.png","PATK+1",50,1,1.5,0));
		getInventory().setBonus();
		updateStats();
		setSkill1(new Whirlwind());
		setSkill2(new LeapAttack());
	}
}
