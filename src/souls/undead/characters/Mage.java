package souls.undead.characters;

import souls.undead.actions.BurningAttack;
import souls.undead.actions.FinalFlash;
import souls.undead.actions.FireBall;
import souls.undead.inventory.Weapon;

public final class Mage extends Pc{
	//CONSTRUCTOR
	public Mage(){
		super("Player","Mage",1,3,1,3,5,5);
		setRange(3);
		setElement(1);
		setAttack(new BurningAttack());
		addSprite(new String[]{"mage_right.png","mage_left.png","mage_up.png","mage_down.png"});
		//addAttackSprite1(new String[]{"mage_atkright1.png","mage_atkleft1.png","mage_atkup1.png","mage_atkdown1.png"});
		addAttackSprite2(new String[]{"mage_atkright2.png","mage_atkleft2.png","mage_atkup2.png","mage_atkdown2.png"});
		setEvasionSprite("mage_evasion.png");
		getInventory().getEquipment().set(0, new Weapon("Wooden Staff","icon_woodenstaff.png","MATK+1",5,1,1.0,1));
		getInventory().setBonus();
		updateStats();
		setSkill1(new FireBall());
		setSkill2(new FinalFlash());
	}
}