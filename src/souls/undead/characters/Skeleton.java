package souls.undead.characters;

import souls.undead.inventory.Consumable;
import souls.undead.inventory.Item;
import souls.undead.utility.Dice;

public final class Skeleton extends Enemy{
	//CONSTRUCTOR
	public Skeleton(){
		super("Skeleton",-1,0,6,1,0,0);
		updateStats();
		addSprite(new String[]{"skeleton_right.png","skeleton_left.png","skeleton_up.png","skeleton_down.png"});
		//addAttackSprite1(new String[]{"skeleton_atkright1.png","skeleton_atkleft1.png","skeleton_atkup1.png","skeleton_atkdown1.png"});
		addAttackSprite2(new String[]{"skeleton_atkright2.png","skeleton_atkleft2.png","skeleton_atkup2.png","skeleton_atkdown2.png"});
		setDirection(Dice.roll(0, 3));
		setSouls(1);
		Item reward = null;
		int r = Dice.roll(0, 2);
		switch(r){
		case 0:
			reward = new Consumable("Health Potion","icon_hpot.png","",10,1,10,0);
			break;
		case 1:
			reward = new Consumable("Mana Potion","icon_mpot.png","",10,1,20,1);
			break;
		case 2:
			reward = new Consumable("Stamina Potion","icon_spot.png","",10,1,5,2);
			break;
		}
		setReward(reward);
	}
}
