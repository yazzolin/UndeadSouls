package souls.undead.characters;

import souls.undead.actions.FinalFlash;

public final class Saiyan extends Pc{
	//CONSTRUCTOR
	public Saiyan(){
		super("Goku","Saiyan",10,10,10,10,10,10);
		updateStats();
		addSprite(new String[]{"saiyan_right.png","saiyan_left.png","saiyan_up.png","saiyan_down.png"});
		//addAttackSprite1(new String[]{"saiyan_atkright1.png","saiyan_atkleft1.png","saiyan_atkup1.png","saiyan_atkdown1.png"});
		addAttackSprite2(new String[]{"saiyan_atkright2.png","saiyan_atkleft2.png","saiyan_atkup2.png","saiyan_atkdown2.png"});
		setEvasionSprite("saiyan_evasion.png");
		setSkill2(new FinalFlash());
	}
}
