package souls.undead.characters;

import java.util.ArrayList;

import souls.undead.map.Map;
import souls.undead.map.Tile;
import souls.undead.utility.Dice;

// Il s'agit des points de t�l�portation
public class Bonfire extends Neutral {
	
	//ATTRIBUTES
	private int location;
	private String[] locationName = {"North-West","North","North-East","West","Center","East","South-West","South","South-East"};
	
	//CONSTRUCTOR
	public Bonfire(int location) {
		super("Bonfire", 1, 0, 0, 0, 0, 0);
		setRole("Object");
		addSprite(new String[]{"","","","bonfire.png"});
		setShopSprite("bonfires.png");
		setDirection(3);
		setLocation(location);
	}
	public String getName(int i){
		return locationName[i];
	}
	public int getLocation(){
		return location;
	}
	public void setLocation(int location){
		this.location=location;
	}
	// Il faut s'assurer que le feu soit plac� sur une case libre, mais aussi qu'il n'y ait pas d'obstacles adjacents
	// pour �viter que le point de t�l�portation se trouve � l'entr�e d'un batiment, ce qui bloquerait l'acc�s
	public void setStartingPosition(ArrayList<Tile> mapdata, ArrayList<Character> charList){
		Boolean ok = false;
		int[]newPos = {0,0};
		while (!ok){
			ok = true;
			int jend = Map.WIDTH/3;
			int kend = Map.HEIGHT/3;
			int[]coords = Map.transformCoords(location,jend,kend);
			int jBig = coords[0];
			int kBig = coords[1];
			newPos[0] = Dice.roll(1, jend-1);
			newPos[1] = Dice.roll(1, kend-1);
			newPos[0] += jBig;
			newPos[1] += kBig;
			for (Tile tile : mapdata){
				if (ok && newPos[0]==tile.getPosition()[0] && newPos[1]==tile.getPosition()[1]){
					ok = tile.isWalkable();
					//System.out.println(tile.getType()+";"+tile.isWalkable());
				}
				if (ok && ((newPos[0]+1==tile.getPosition()[0] && newPos[1]==tile.getPosition()[1]) || (newPos[0]-1==tile.getPosition()[0] && newPos[1]==tile.getPosition()[1]) || (newPos[0]==tile.getPosition()[0] && newPos[1]+1==tile.getPosition()[1]) || (newPos[0]==tile.getPosition()[0] && newPos[1]-1==tile.getPosition()[1]))){
					//System.out.println(tile.getType()+";"+tile.isWalkable());
					ok = tile.isWalkable();
				}
			}
			for (Character c : charList){
				if (newPos[0]==c.getPosition()[0] && newPos[1]==c.getPosition()[1]){
					ok = false;
				}
			}
		}
		System.out.println("bonfire at ("+newPos[0]+","+newPos[1]+")");
		setPosition(newPos[0],newPos[1]);
	}	
}
