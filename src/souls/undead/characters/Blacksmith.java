package souls.undead.characters;

import java.util.ArrayList;

import souls.undead.inventory.Item;
import souls.undead.inventory.Weapon;

public class Blacksmith extends Neutral{
	//ATTRIBUTES
	
	
	//CONSTRUCTOR
	public Blacksmith(){
		super("Smithy",5,20,20,20,1,10);
		updateStats();
		addSprite(new String[]{"","","","npc_blacksmith.png"});
		setShopSprite("shop_blacksmith.png");
		setDirection(3);
		setSouls(5);
		initShop();
	}

//=====GETTERS=&=SETTERS=====//
	
	public ArrayList<Item> getShop() {
		return shop;
	}
	public void initShop(){
		shop.add(new Weapon("Bandit's Knife","icon_knife.png","Phys ATK +1",5,1,4.0,0));
		shop.add(new Weapon("Broadsword","icon_sword.png","Phys ATK +2",10,2,3.0,0));
		shop.add(new Weapon("Scimitar","icon_scimitar.png","Phys ATK +3",20,3,3.8,0));
		shop.add(new Weapon("Rapier","icon_rapier.png","Phys ATK +4",30,4,3.5,0));
		shop.add(new Weapon("Uchigatana","icon_ironkatana.png","Phys ATK +5",50,5,2.0,0));
		shop.add(new Weapon("Naginata","icon_naginata.png","Phys ATK +6",60,6,1.5,0));
		shop.add(new Weapon("Ruler's Sword","icon_greatsword.png","Phys ATK +9",90,9,0.95,0));
		shop.add(new Weapon("Dragon Spear","icon_spear.png","Phys ATK +12",120,12,0.90,0));
		shop.add(new Weapon("Battle Axe","icon_axe.png","Phys ATK +16",160,16,0.80,0));
		shop.add(new Weapon("King's Hammer","icon_hammer.png","Phys ATK +20",900,20,0.7,0));
		shop.add(new Weapon("Greatsword","icon_ugs.png","Phys ATK +40",300,40,0.5,0));
		shop.add(new Weapon("Longbow","icon_bow.png","Phys ATK+2",50,10,1.2,0));	
	}
	
} // end of class
