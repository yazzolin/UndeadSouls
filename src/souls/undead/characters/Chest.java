package souls.undead.characters;

import souls.undead.inventory.Consumable;
import souls.undead.inventory.Item;
import souls.undead.utility.Dice;

public class Chest extends Neutral{
	public Chest(){
		super("Chest",1,0,0,0,0,0);
		setRole("Object");
		addSprite(new String[]{"","","","icon_chest.png"});
		setDirection(3);
		setSouls(0);
		int ran=Dice.roll(0,2);
		switch(ran){
		case 0:
			setReward(new Consumable("Health Potion","icon_hpot.png","",10,5,10,0));
			break;
		case 1:
			setReward(new Consumable("Mana Potion","icon_mpot.png","",10,5,20,1));
			break;
		case 2:
			setReward(new Consumable("Stamina Potion","icon_spot.png","",10,5,5,2));
			break;
		}
	}
	
	public Chest(Item item){
		super("Chest",1,0,0,0,0,0);
		setRole("Object");
		addSprite(new String[]{"","","","icon_chest.png"});
		setDirection(3);
		setReward(item);
	}

}
