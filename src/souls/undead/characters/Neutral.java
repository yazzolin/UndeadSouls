package souls.undead.characters;

import java.util.ArrayList;

import souls.undead.inventory.Item;

public abstract class Neutral extends Npc{
	
	//ATTRIBUTES
	protected boolean hostile = false;
	protected ArrayList<Item> shop = new ArrayList<Item>();
	protected String shopSprite;
	
	//CONSTRUCTOR
	public Neutral(String name, int vit, int end, int str, int dex, int pow, int itl){
		super(name,"Neutral",vit,end,str,dex,pow,itl);
	}

//=====GETTERS=&=SETTERS=====//

	public boolean isHostile() {
		return hostile;
	}
	public void setHostile(boolean hostile) {
		this.hostile = hostile;
	}
	public ArrayList<Item> getShop() {
		return shop;
	}

	public String getShopSprite() {
		return shopSprite;
	}

	public void setShopSprite(String shopSprite) {
		this.shopSprite = shopSprite;
	}
	
	
	
}
