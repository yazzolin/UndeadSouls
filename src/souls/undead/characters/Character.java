package souls.undead.characters;

import java.io.Serializable;
import java.util.ArrayList;
import souls.undead.actions.*;
import souls.undead.inventory.Inventory;
import souls.undead.inventory.Item;
import souls.undead.map.Map;
import souls.undead.map.Tile;
import souls.undead.mvc.Game;
import souls.undead.threads.SpRegen;
import souls.undead.utility.Dice;

/**
 * Classe abstraite contenant toutes les informations commune aux diff�rents
 * personnages du jeu, que ce soit le joueur ou les monstres qu'il affronte.
 */
public abstract class Character implements Serializable{	
	
	private static final long serialVersionUID = 1L;
	
	//INTERFACES
	protected Attack attack = new BasicAttack();
	protected transient Dodge dodge = new Evasion1();
	protected transient Hurt hurt = new NormalHurt();
	protected Move move = new NormalMove();
	protected Inventory inventory = new Inventory();
	protected Skill skill1 = new Whirlwind();
	protected Skill skill2 = new LeapAttack();
	
	//STATS
	protected String name = "";
 	protected String role = "";
	protected int level = 1;
	protected int souls = 0; // Nombre d'�mes (monnaie et exp�rience du jeu)
	protected int hp;
	protected int mhp; // Points de vie maximum
	protected int mp;
	protected int mmp; // Points de mana maximum
	protected int sp;
	protected int msp; // Points de stamina maximum
	protected double as; // Vitesse d'attaque
	protected double ms; // Vitesse de d�placement
	protected int pdef; // Resistance aux d�g�ts physiques
	protected int mdef; // Resistance aux d�g�ts magiques
	protected int vitality;
	protected int endurance;
	protected int strength;
	protected int agility; 
	protected int power;
	protected int intelligence;
	
	protected int range = 1; // Port�e de l'attaque de base
	protected int element = 0; // Element de l'attaque de base (0 = physique, 1 = magique)
	protected int spRegenDelay = 700; // Delai en ms de la r�g�n�ration de stamina
	
	//MISCELLANOUS
	protected transient Thread thread = null;
	protected transient Game game;
	protected int[] position = {0,0};
	protected transient boolean inAction = false;
	protected transient boolean invincible = false;
	protected transient boolean isHurt = false;
	protected transient int skillNum = 0; // Pour d�terminer l'effet visuel associ� au sort lanc�
	protected transient int status = 0; // 0 = normal, 1 = br�l�
	
	//SPRITES
	protected String evasionSprite;
	//protected ArrayList<String> attackSprites1 = new ArrayList<String>();
	protected ArrayList<String> attackSprites2 = new ArrayList<String>();
	protected ArrayList<String> sprites = new ArrayList<String>();
	protected int direction = 0; // Direction dans laquelle le personnage fait face 0=droite 1=gauche 2=haut =3bas
	
	//CONSTRUCTOR
	public Character(){}
	
	public Character(String name, String role, int vit, int end, int str, int agi, int pow, int itl){
		setName(name);
		setRole(role);
		setVitality(vit);
		setEndurance(end);
		setStrength(str);
		setAgility(agi);
		setPower(pow);
		setIntelligence(itl);
	}
	
	// Appel� apr�s la mort du joueur
	public void respawn(Game game){
		this.game = game;
		this.inAction = false;
		this.invincible = false;
		this.isHurt = false;
		this.hurt = new NormalHurt();
		this.dodge = new Evasion1();
		this.hp = mhp;
		this.mp = mmp;
		this.sp = msp;
		this.status = 0;
		this.thread = new Thread(new SpRegen(this));

	}
	//=====THREAD=====//
	public void staminaRegen(){
		try{
			if (getThread().getState().equals(Thread.State.TERMINATED))
				setThread( new Thread(new SpRegen(this)) );
			getThread().start();
		}catch(NullPointerException | IllegalThreadStateException e){}
	}
	
	//=====PLAYER=ACTIONS=====//
	public void attack(){
		attack.damageDealt(this);
	}
	public void dodge(){
		dodge.avoidDamage(this);
	}	
	public void move(int dx, int dy){
		move.move(this,dx,dy);
	}
	public void hurt(int damage, int element){
		hurt.damageTaken(this, damage, element);
	}	
	public void killed(){
		game.removeNpc(this);
	}		
	public Character interact(){
		Character npc = null;
		npc = getGame().checkAttackCollision(position[0], position[1], getDirection(), 1, false).get(0);
		return npc;
	}
	public void useSkill(int i) {
		switch(i){
		case 0:
			skill1.useSkill(this);
			break;
		case 1:
			skill2.useSkill(this);
			break;
		}
	}
	
	//ABSTRACT
	public abstract void levelUp();
	public abstract void useItem(Item item);
	public abstract void useQuickItem(int slot);
	public abstract void soulsReward(Character c);
	public abstract void itemReward(Character c);
	//public abstract void buildAggroZone(ArrayList<Tile> mapdata);
	
//==========GETTERS=&=SETTERS==========//
	
	//=====PLAYER=ACTIONS=====//
//	public Attack getAttack() {
//		return attack;
//	}
	public void setAttack(Attack attack) {
		this.attack = attack;
	}
//	public Dodge getDodge() {
//		return dodge;
//	}
	public void setDodge(Dodge dodge) {
		this.dodge = dodge;
	}
	public void setHurt(Hurt hurt) {
		this.hurt = hurt;
	}
//	public Move getMove() {
//		return move;
//	}
	public void setMove(Move move) {
		this.move = move;
	}
	public void setSkill1(Skill skill1){
		this.skill1=skill1;
	}
	public void setSkill2(Skill skill2){
		this.skill2=skill2;
	}	
	public Skill getSkill1() {
		return skill1;
	}
	public Skill getSkill2() {
		return skill2;
	}

	//=====PLAYER=INFO=====//
	public void updateStats(){
		//int bonus[] = inventory.getBonus();
		mhp = 10 + vitality*3;
		hp = mhp;
		msp = 10 + endurance*2;
		sp = msp;
		mmp = 10 + intelligence*5;
		mp = mmp;
		pdef = endurance/2;
		mdef = intelligence/2;
		if (this instanceof Pc){
			setMovementSpeed(1.0 + agility*0.1);
			setAttackSpeed(getInventory().getWeaponSpeed());
			getInventory().setCapacity(4+strength/2);
		}
	}
		
	public String getName(){
		return this.name;
	}	
	public void setName(String name){
		this.name = name;
	}	
	public String getRole(){
		return this.role;
	}
	public void setRole(String role){
		this.role = role;
	}
	public int[] getPosition() {
		return this.position;
	}	
	public void setPosition(int x,int y) {
		this.position[0]=x;
		this.position[1]=y;
	}
	/**
	 * Impose al�atoirement la position o� le personnage va appara�tre,
	 * en veillant � ce qu'il ne soit pas sur un mur par exemple.
	 */
	public void setStartingPosition(ArrayList<Tile> mapdata, ArrayList<Character> charList){
		Boolean ok = false;
		int[] newPos = {0,0};
		while (!ok){
			newPos[0] = Dice.roll(1, Map.WIDTH-1);
			newPos[1] = Dice.roll(1, Map.HEIGHT-1);
			for (Tile tile : mapdata){
				if (newPos[0]==tile.getPosition()[0] && newPos[1]==tile.getPosition()[1]){
					ok = tile.isWalkable();
				}
			}
			for (Character c : charList){
				if (newPos[0]==c.getPosition()[0] && newPos[1]==c.getPosition()[1]){
					ok = false;
				}
			}
		}
		setPosition(newPos[0],newPos[1]);
	}	
	/**
	 * Fait en sorte que le personnage puisse appairaitre uniquement sur un type de case, tout en �vitant
	 * d'appara�tre sur une case d�j� occup�e.
	 */
	public void setSpecialStartingPosition(ArrayList<Tile> mapdata, ArrayList<Character> charList, String tileType){
		boolean ok = false;
		int[] newPos = {0,0};
		while(!ok){
			newPos[0] = Dice.roll(1, Map.WIDTH-1);
			newPos[1] = Dice.roll(1, Map.HEIGHT-1);
			for (Tile tile : mapdata){
				if (newPos[0]==tile.getPosition()[0] && newPos[1]==tile.getPosition()[1] && tile.getType().equals(tileType)){
					ok = true;
					//System.out.println("true");
				}
			}
			for (Character c : charList){
				if (newPos[0]==c.getPosition()[0] && newPos[1]==c.getPosition()[1]){
					ok = false;
				}
			}
		}
		setPosition(newPos[0],newPos[1]);
	}
	
	public void reachBonfire(){
		setHp(getMhp());
		setMp(getMmp());
		setSp(getMsp());
		game.repaint();
	}
	
	public void teleport(){
		int index = (game.getMenuSelect()-145)/25;
		Bonfire bonfire = game.getData().getBonfires().get(index);
		setPosition(bonfire.getPosition()[0]+1, bonfire.getPosition()[1]);
		game.save();
		game.refreshView();
	}
	public boolean isInAction() {
		return this.inAction;
	}	
	public void setInAction(boolean inAction){
		this.inAction = inAction;
		game.repaint();
	}	
	
	//=====SPRITES=====//
	public void replaceSprite(int direction, String sprite){
		this.sprites.set(direction, sprite);
		game.repaint();
	}
	public String getSprite(){
		return this.sprites.get(this.direction);
	}	
	public void addSprite(String[] sprites){
		for(String element : sprites){
			this.sprites.add(element);
		}
	}
//	public void addAttackSprite1(String[] sprites){
//		for(String element : sprites){
//			this.attackSprites1.add(element);
//		}
//	}
	public void addAttackSprite2(String[] sprites){
		for(String element : sprites){
			this.attackSprites2.add(element);
		}
	}
	public int getDirection(){
		return this.direction;
	}	
	public void setDirection(int direction){
		this.direction = direction;
	}		

	public String getEvasionSprite() {
		return evasionSprite;
	}
	public void setEvasionSprite(String evasionSprite) {
		this.evasionSprite = evasionSprite;
	}
//	public String getAttackSprite1(){
//		return this.attackSprites1.get(this.direction);
//	}
	public String getAttackSprite2(){
		return this.attackSprites2.get(this.direction);
	}
	
	//======MISC=====//
	public Game getGame() {
		return game;
	}
	public void setGame(Game game) {
		this.game = game;
	}
	public Thread getThread() {
		return thread;
	}
	public void setThread(Thread thread) {
		this.thread = thread;
	}

	//=====STATS=====//
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public int getSouls() {
		return souls;
	}
	public void setSouls(int souls) {
		if (souls<0){
			this.souls = 0;
		}else{
			this.souls = souls;
		}
	}
	public int getHp() {
		return this.hp;
	}
	public void setHp(int hp) {
		if (hp>mhp)
			this.hp = mhp;
		else
			this.hp = hp;
	}
	public int getMhp() {
		return this.mhp;
	}
	public void setMhp(int mhp) {
		this.mhp = mhp;
	}	
	public int getMp() {
		return this.mp;
	}
	public void setMp(int mp) {
		if (mp>mmp)
			this.mp = mmp;
		else if (mp<0)
			this.mp = 0;
		else
			this.mp = mp;
	}
	public int getMmp() {
		return this.mmp;
	}
	public void setMmp(int mmp) {
		this.mmp = mmp;
	}		
	public int getSp() {
		return sp;
	}
	public void setSp(int sp) {
		if (sp>msp)
			this.sp = msp;
		else if (sp<0)
			this.sp = 0;
		else
			this.sp = sp;
	}
	public int getMsp() {
		return msp;
	}
	public void setMsp(int msp) {
		this.msp = msp;
	}
	public boolean isAlive() {
		return hp>0;
	}
	public double getAttackSpeed() {
		return as;
	}
	public void setAttackSpeed(double as) {
		if (as>5.0)
			as = 5.0;
		this.as = as;
	}
	public double getMovementSpeed() {
		return ms;
	}
	public void setMovementSpeed(double ms) {
		this.ms = ms;
	}
	public int getPdef() {
		return pdef;
	}
	public void setPdef(int pdef) {
		this.pdef = pdef;
	}
	public int getMdef() {
		return mdef;
	}
	public void setMdef(int mdef) {
		this.mdef = mdef;
	}
	public int getVitality() {
		return vitality;
	}
	public void setVitality(int vitality) {
		this.vitality = vitality;
	}
	public int getEndurance() {
		return endurance;
	}
	public void setEndurance(int endurance) {
		this.endurance = endurance;
	}
	public int getStrength() {
		return strength;
	}
	public void setStrength(int strength) {
		this.strength = strength;
	}
	public int getAgility() {
		return agility;
	}
	public void setAgility(int agility) {
		this.agility = agility;
	}
	public int getPower() {
		return power;
	}
	public void setPower(int power) {
		this.power = power;
	}
	public int getIntelligence() {
		return intelligence;
	}
	public void setIntelligence(int intelligence) {
		this.intelligence = intelligence;
	}	
	public int getSpRegenDelay() {
		return spRegenDelay;
	}
	public void setSpRegenDelay(int spRegenDelay) {
		if (spRegenDelay<200)
			spRegenDelay = 200;
		this.spRegenDelay = spRegenDelay;
	}
	public int getRange() {
		return range;
	}
	public void setRange(int range) {
		if (range<1)
			range = 1;
		this.range = range;
	}
	public int getElement() {
		return element;
	}
	public void setElement(int element) {
		this.element = element;
	}
	public boolean isInvincible() {
		return invincible;
	}
	public void setInvincible(boolean invincible) {
		this.invincible = invincible;
	}
	public boolean isHurt() {
		return isHurt;
	}
	public void setIsHurt(boolean isHurt) {
		this.isHurt = isHurt;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	public int getSkillNum() {
		return skillNum;
	}

	public void setSkillNum(int skillNum) {
		this.skillNum = skillNum;
	}

	//=====INVENTORY=====//
	public Inventory getInventory(){
		return inventory;
	}
	public int[] getBonus(){
		return inventory.getBonus();
	}


		
}// end of class
