package souls.undead.characters;

import souls.undead.inventory.Item;
import souls.undead.utility.Gamelog;

public abstract class Pc extends Character{

	//ATTRIBUTES	
	protected int uppoints = 0;

	//CONSTRUCTOR
	public Pc(String name, String role, int vit, int end, int str, int agi, int pow, int itl){
		super(name,role,vit,end,str,agi,pow,itl);
	}
	
	public void useItem(Item item){
		getInventory().useItem(this, item);
	}
	
	public void useQuickItem(int slot){
		try{
			getInventory().useQuickItem(this, slot);
		}catch (ClassCastException e){}
	}
	
	public void buyItem(Item item){
		if (getSouls()<item.getValue())
			Gamelog.addText("You don't have enough souls");
		else{
			if (!getInventory().isFull()){
				Gamelog.addText(item.getValue()+" souls spent");
				setSouls(getSouls()-item.getValue());
				getInventory().addItem(item);
			}else{
				Gamelog.addText("Inventory is full");
			}
		}
	}
	
	public void levelUp(){
		if (getLevel()!=200){
			int up = level*2;//(int)(level + (level*level)/1.5)+2;
			if (getSouls()>=up){
				setSouls(getSouls() - up);
				setLevel(level+1);
				setUpPoints(getUpPoints()+1);
				Gamelog.addText(Integer.toString(up)+" Souls consumed");
				Gamelog.addText(getName()+"'s Soul Level is now "+getLevel());
				Gamelog.addText("1 Up points acquired");
			}else{
				Gamelog.addText(Integer.toString((up-getSouls()))+" more souls needed to level up");
			}
		}else{
			Gamelog.addText("Soul level limit reached");
		}
		game.repaint();
	}
	public void levelUpStat(int n){
		if (this.getUpPoints()>0){
			int up[] = new int[6];
			up[n] = 1;
			setVitality(getVitality()+up[0]);
			setEndurance(getEndurance()+up[1]);
			setStrength(getStrength()+up[2]);
			setAgility(getAgility()+up[3]);
			setPower(getPower()+up[4]);
			setIntelligence(getIntelligence()+up[5]);
			updateStats();
			setUpPoints(getUpPoints()-1);
			game.repaint();
		}
	}
	//ABSTRACT
	public void soulsReward(Character c){}
	public void itemReward(Character c){};
//==========GETTERS=&=SETTERS==========//	
	public int getUpPoints() {
		return uppoints;
	}
	public void setUpPoints(int uppoints) {
		this.uppoints = uppoints;
	}
	
}// end of class
