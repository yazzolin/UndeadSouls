package souls.undead.characters;

import java.util.ArrayList;

import souls.undead.inventory.Item;
import souls.undead.inventory.Weapon;

public final class Sorceress extends Neutral{
	public Sorceress(){
		super("Sorceress",15,15,10,10,70,50);
		updateStats();
		addSprite(new String[]{"","","","npc_sorceress.png"});
		setShopSprite("shop_blacksmith.png");
		setDirection(3);
		setSouls(15);
		initShop();
	}
//=====GETTERS=&=SETTERS=====//
		public ArrayList<Item> getShop() {
			return shop;
		}
		public void initShop(){
			shop.add(new Weapon("Crystal staff","icon_crystalstaff.png","MATK +6",35,6,1.75,1));
			shop.add(new Weapon("Abyss staff","icon_abyssstaff.png","MATK +10",55,10,1.5,1));
			shop.add(new Weapon("King staff","icon_kingstaff.png","MATK +20",90,20,0.75,1));
		}
	
}
