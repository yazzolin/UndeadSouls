package souls.undead.characters;

import java.util.ArrayList;

import souls.undead.inventory.Item;
import souls.undead.inventory.NoItem;
import souls.undead.map.Tile;
import souls.undead.mvc.Data;
import souls.undead.mvc.Game;
import souls.undead.utility.Dice;
import souls.undead.utility.Gamelog;
import souls.undead.utility.Tool;

public abstract class Npc extends Character{
	//ATTRIBUTES
	protected int aggroRange = 4+Data.DIFFICULTY*2;
	protected Item reward = null;
	protected int rewardRarity = 3;
	
	//CONSTRUCTOR
	public Npc(String name, String role, int vit, int end, int str, int agi, int pow, int itl){
		super(name,role,vit,end,str,agi,pow,itl);
		levelUp();
	}
		
	//ABSTRACT
	public void useItem(Item item){}
	public void useQuickItem(int slot){}
	public void buildAggroZone(ArrayList<Tile> mapdata){}
	public void levelUp(){
		setVitality(getVitality()+Data.DIFFICULTY);
		setEndurance(getEndurance()+Data.DIFFICULTY);
		setStrength(getStrength()+Data.DIFFICULTY);
		setAgility(getAgility()+Data.DIFFICULTY);
		setPower(getPower()+Data.DIFFICULTY);
		setIntelligence(getIntelligence()+Data.DIFFICULTY);
		setAttackSpeed(getAgility()/10.0 + Data.DIFFICULTY/10.0);
		setMovementSpeed(getAgility()/5.0 + Data.DIFFICULTY/5.0);
	}
	public void soulsReward(Character c){
		int sr = this.getSouls() * Data.DIFFICULTY;
		if (c.getSouls()<999999){
			c.setSouls(c.getSouls()+sr);
			if (sr<=1)
				Gamelog.addText(c.getName()+" has retrieved "+sr+" soul");
			else
				Gamelog.addText(c.getName()+" has retrieved "+sr+" souls");
		}else{
			Gamelog.addText("Can't hold more souls !");
		}
	}	
	public void itemReward(Character c){
		if (getReward()!=null && !c.getInventory().isFull()){
			for (Item item : c.getInventory().getPouch()){
				if (item instanceof NoItem){
					c.getInventory().getPouch().set(c.getInventory().getPouch().indexOf(item), getReward());
					break;
				}
			}	
			Gamelog.addText(getReward().getName()+" obtained");
			setReward(null);
		}else if (c.getInventory().isFull()){
			Gamelog.addText("Inventory is full");
		}
	}
	
	//OPERATIONS
	public void rewardDropChance(){
		if (reward!=null){
			int d = Dice.roll(0, rewardRarity*(Data.DIFFICULTY-1));
			if (d==0){
				Chest drop = new Chest(getReward());
				drop.setPosition(getPosition()[0], getPosition()[1]);
				getGame().addRewardDrop(drop);
			}
		}
	}
	public int getAggroRange() {
		return aggroRange;
	}
	public void setAggroRange(int aggroRange) {
		this.aggroRange = aggroRange;
	}
	public boolean inAggroRange(){
		boolean ok = false;
		try{
			ok = (Tool.euclideanDistance(getPosition(), getGame().getData().getPlayer().getPosition())<=getAggroRange());
		}catch(NullPointerException e){}
		return ok;
	}
	public Item getReward() {
		return reward;
	}
	public void setReward(Item reward) {
		this.reward = reward;
	}
	public void setRewardRarity(int rewardRarity) {
		if (rewardRarity<0)
			rewardRarity = 1;
		else if (rewardRarity>100)
			rewardRarity = 100;
		this.rewardRarity = rewardRarity;
	}
	
	
}// end of class
