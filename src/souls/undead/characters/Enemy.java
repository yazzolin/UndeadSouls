package souls.undead.characters;

import java.util.ArrayList;

import souls.undead.map.Tile;
import souls.undead.mvc.Game;
import souls.undead.threads.EnemyThread;
import souls.undead.utility.Dice;
import souls.undead.utility.Gamelog;
import souls.undead.utility.Node;
import souls.undead.utility.Pathfinding;

public abstract class Enemy extends Npc{
	//ATTRIBUTES
	protected Boolean boss = false;
	protected transient Pathfinding pathfinding;
	protected transient ArrayList<Node> path= new ArrayList<Node>();
	protected transient ArrayList<Node> aggroZone = new ArrayList<Node>();
	protected transient int[] playerPosition;
	
	//CONSTRUCTOR
	public Enemy(String name, int vit, int end, int str, int agi, int pow, int itl){
		super(name,"Enemy",vit,end,str,agi,pow,itl);
	}
	
	//THREADS
	
	public void updateThread(){
		if (getRole().equals("Enemy")){
			if (getThread()!=null){
				//System.out.println(getThread().getState()+":"+getThread().getName());
				switch(getThread().getState()){
				case NEW:
					try{
						if(!getThread().isAlive())
							getThread().start();
					}catch(IllegalThreadStateException e){
						System.out.println("hi");
					}
					break;
				case RUNNABLE:
					break;
				case TERMINATED:
					if(inAggroRange())
						setThread(new Thread(new EnemyThread(this)));
					break;
				case BLOCKED:
					try{getThread().join();}catch(InterruptedException e){}
					break;
				case WAITING:
					break;
				case TIMED_WAITING:
					break;
				}
			}else{
				setThread(new Thread(new EnemyThread(this)));
			}
		}
	}
	//PATHFINDING
	public void buildAggroZone(ArrayList<Tile> mapdata){
		this.playerPosition=game.getData().getPlayer().getPosition();
		Node goalNode = null;
		Node startNode = null;
		this.aggroZone.clear();
		int x = this.getPosition()[0];
		int y = this.getPosition()[1];
		boolean ok = false;
		//System.out.println("hellow 1");
		for(Tile element : mapdata){
			if(element.getPosition()[0]>=x-this.aggroRange && element.getPosition()[0]<=x+this.aggroRange && element.getPosition()[1]>=y-this.aggroRange && element.getPosition()[1]<=y+this.aggroRange){
				//System.out.println(element.getPosition()[0] +" "+ element.getPosition()[1]);
				//System.out.println("hellow 2");
				if(!element.isWalkable()){
					//System.out.println("inside Wall "+element.getPosition()[0]);
					this.aggroZone.add(new Node(element.getPosition()[0],element.getPosition()[1],100000));
					//System.out.println("hellow WALL");
					}
				else if(game.checkCharCollision(element.getPosition()[0],element.getPosition()[1])&&(element.getPosition()[0]!=this.getPosition()[0] || element.getPosition()[1]!=this.getPosition()[1])){
					//System.out.println("hellew at "+element.getPosition()[0]+" "+element.getPosition()[1]);
					this.aggroZone.add(new Node(element.getPosition()[0],element.getPosition()[1],100000));
				}
				else{
					this.aggroZone.add(new Node(element.getPosition()[0],element.getPosition()[1]));
					//System.out.println("hellow NOT WALL");
					if(element.getPosition()[0]==playerPosition[0]&&element.getPosition()[1]==playerPosition[1]){
						//System.out.println("hellow Player position found! = "+ playerPosition[0]+" "+ playerPosition[1]);
						goalNode=this.aggroZone.get(this.aggroZone.size()-1);
						//System.out.println("goal cost = "+ goalNode.getCost());
					}
					if(element.getPosition()[0]==this.getPosition()[0] && element.getPosition()[1]==this.getPosition()[1]){
						//System.out.println("this found");
						startNode = this.aggroZone.get(this.aggroZone.size()-1);
						startNode.setCost(0);
					}
				}
			}
		}
		pathfinding = new Pathfinding(startNode,goalNode,aggroZone);
		path=pathfinding.path();
		//System.out.println("goal cost = "+ goalNode.getCost());
		//System.out.println("path size = "+path.size());
	}
	
//=====GETTERS=&=SETTERS=====//
	public ArrayList<Node> getPath() {
		return path;
	}
	public void setPath(ArrayList<Node> path) {
		this.path = path;
	}
	public Pathfinding getPathfinding() {
		return pathfinding;
	}
	public void setPathfinding(Pathfinding pathfinding) {
		this.pathfinding = pathfinding;
	}
	public ArrayList<Node> getAggroZone() {
		return aggroZone;
	}
	public void setAggroZone(ArrayList<Node> aggroZone) {
		this.aggroZone = aggroZone;
	}
	public int[] getPlayerPosition() {
		return playerPosition;
	}
	public void setPlayerPosition(int x,int y) {
		int []position={x,y};
		this.playerPosition = position;
	}
}
