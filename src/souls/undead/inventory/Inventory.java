package souls.undead.inventory;

import java.io.Serializable;
import java.util.ArrayList;
import souls.undead.characters.Character;
import souls.undead.utility.Gamelog;

public class Inventory implements Serializable{

	private static final long serialVersionUID = 1L;
	//======ATTTRIBUTES=====//
	private ArrayList <Item> equipment = new ArrayList<Item>(3); // Contient les objets �quip�s par le personnage
	private ArrayList <Item> shortcuts = new ArrayList<Item>(5); // Contient les raccourcis assign�s par le joueur
	private ArrayList <Item> pouch = new ArrayList<Item>(22); // Contient tous les objets de l'inventaire du personnage
	private int[] bonus = new int[15]; // Contient les bonus donn�s par les �quipements
	private int capacity; // Le nombre d'ojbets que peut contenir l'inventaire actuel du personnage
	
	private final static int MAXCAPACITY = 22; // Le nombre maximum d'objets que peut contenir l'inventaire
	private final static int MINCAPACITY = 5; // Le nombre minimum d'objets que peut contenir l'inventaire
	public final static int LINELENGTH = 11; // Le nombre maximum d'objets par ligne, dans l'inventaire
	
	//CONSTRUCTOR
	public Inventory(){
//		pouch.add(new Weapon("Iron Katana","icon_ironkatana.png","PATK+10, BLEED Lv1",100,10,0));
//		//pouch.add(new Weapon("Wooden Sword","icon_bokutou.png","PATK+3, STUN Lv1",50,3,0));
//		pouch.add(new Armor("Iron Armor","icon_ironarmor.png","PDEF+2",100,2,0));
//		//pouch.add(new Armor("Magic Armor","icon_magicarmor.png","PDEF+2, MDEF+2",100,2,2));
		pouch.add(new Consumable("Health Potion","icon_hpot.png","",10,5,10,0));
		pouch.add(new Consumable("Mana Potion","icon_mpot.png","",10,5,20,1));
		pouch.add(new Consumable("Stamina Potion","icon_spot.png","",10,5,5,2));
		//pouch.add(new Weapon("Crystal staff","icon_crystalstaff.png","MATK +6",70,6,1.75,1));
		//pouch.add(new Ring("REDTEARSTONE RING","icon_ironring.png","DMGx2 IF HP BELOW 20%",100));
		int i;
		for (i=0;i<5;i++)
			shortcuts.add(new NoItem());
		for (i=0;i<3;i++)
			equipment.add(new NoItem());
	}
	
//==========GETTERS=&=SETTERS==========//	
	public boolean isFull(){
		return getPouchSize()==capacity;
	}
	public void setCapacity(int capacity) {
		if (capacity>MAXCAPACITY)
			capacity = MAXCAPACITY;
		if (capacity<MINCAPACITY)
			capacity = MINCAPACITY;
		this.capacity = capacity;
		updateCapacity();
	}
	public void updateCapacity(){
		int i;
		ArrayList<Item> newpouch = new ArrayList<Item>();
		for (i=0;i<pouch.size();i++){
			Item item = pouch.get(i);
//			if (!(item instanceof NoItem))
			//if (!(item.getClass().isAssignableFrom(NoItem.class.getClass())))
			if (!item.getName().equals("None"))
				newpouch.add(item);
		}				
		pouch = newpouch;
		int noitem = capacity - pouch.size();
		for (i=0;i<noitem;i++)
			pouch.add(new NoItem());		
	}
	/*
	 * Utiliser ArrayList.size() compterait �galement les objets instanci�s de NoItem, 
	 * c'est pourquoi il faut une m�thode qui les ignore.
	 */
	private int getPouchSize(){
		int count = 0;
		for (Item item : pouch){
			//if (!(item instanceof NoItem))
			if (!item.getName().equals("None"))
				count++;
		}
		return count;
	}

	public int[] getBonus(){
		return this.bonus;
	}	
	public void setBonus(){
		for (int i=0;i<bonus.length;i++){
			this.bonus[i]=0;
		}
		//if (!(equipment.get(0) instanceof NoItem)){
		if (!(equipment.get(0).getName().equals("None"))){
			if (((Weapon) equipment.get(0)).getElement()==0)
				bonus[11] += ((Weapon) equipment.get(0)).getDamage();
			else
				bonus[12] += ((Weapon) equipment.get(0)).getDamage();
		}
		//if (!(equipment.get(1) instanceof NoItem)){
		if (!(equipment.get(1).getName().equals("None"))){
			bonus[13] += ((Armor) equipment.get(1)).getPdef();
			bonus[14] += ((Armor) equipment.get(1)).getMdef();
		}
	}
	public ArrayList<Item> getEquipment(){
		return this.equipment;
	}	
	public ArrayList<Item> getPouch(){
		return this.pouch;
	}
	public ArrayList<Item> getShortcuts(){
		return this.shortcuts;
	}	
	public double getWeaponSpeed(){
		double speed = 1.0;
		try{
			speed = ((Weapon) equipment.get(0)).getSpeed(); 
		}catch(ClassCastException e){}
		return speed;
	}
	//=====INVENTORY=MANAGEMENT=====//
	public void useItem(Character c, Item item){
		consumeItem(c,item);			
	}
	
	public void useQuickItem(Character c, int slot){
		consumeItem(c,getShortcuts().get(slot));

	}	
	private void consumeItem(Character c, Item item){
		Gamelog.addText(item.getName()+" consumed");
		switch(((Consumable) item).getCategory()){
		case 0:
			Gamelog.addText( ((Consumable) item).getPower() +" Health Points restored");
			c.setHp(c.getHp()+ ((Consumable) item).getPower());
			break;
		case 1:
			Gamelog.addText( ((Consumable) item).getPower() +" Mana Points restored");
			c.setMp(c.getMp()+ ((Consumable) item).getPower());
			break;
		case 2:
			Gamelog.addText( ((Consumable) item).getPower() +" Stamina Points restored");
			c.setSp(c.getSp()+ ((Consumable) item).getPower());
			break;
		}
		throwItem(item);
	}
	// Place le nouvel � la place d'un NoItem (= emplacement libre)
	public void addItem(Item item){
		for (Item element : pouch){
			if (element.getName().equals("None")){
				pouch.set(pouch.indexOf(element), item);
				break;
			}
		}
		Gamelog.addText(item.getName()+" added to inventory");
	}
	// Equipe l'objet choisi, et �change de place avec un �ventuel objet d�j� �quip� pr�c�demment.
	public void equipItem(int pos, Item item){
		int n=-1;
		if (item instanceof Weapon)
			n=0;
		else if (item instanceof Armor)
			n=1;
		else if (item instanceof Ring)
			n=2;
		
		if (n!=-1){
			//if (equipment.get(n) instanceof NoItem)
			if (equipment.get(n).getName().equals("None"))
				pouch.set(pouch.indexOf(item), new NoItem());
			else
				pouch.set(pos, equipment.get(n));
			equipment.set(n, item);}
		setBonus();
	}
	// Jette un objet tout en le retirant des raccourcis, si n�cessaire.
	public void throwItem(Item item){
		if (!item.getName().equals("None")){
			try{
				if(((Consumable) item).isQuickAcces())
					removeShortcut(item);
			}catch(ClassCastException e){}
			pouch.set(pouch.indexOf(item), new NoItem());
			Gamelog.addText(item.getName()+" has vanished");
		}
	}
	// Place un ojbet en raccourci uniquement si l'emplacement choisi est libre.
	public void setShortcut(int slot, Item item){
		try{
			if (item instanceof Consumable && !(shortcuts.get(slot) instanceof Consumable)){
				if(((Consumable) item).isQuickAcces()){
					Item oldSlot = new NoItem();
					shortcuts.set(shortcuts.indexOf(item), oldSlot);
				}
					shortcuts.set(slot, ((Consumable) item));
					((Consumable) item).setQuickAcces(true);
			}
		}catch (NullPointerException e){}
	}

	public void removeShortcut(Item item){
		try{
			shortcuts.set(shortcuts.indexOf(item), new NoItem());
			((Consumable) item).setQuickAcces(false);
		}catch (NullPointerException e){}
	}

}// end of class
