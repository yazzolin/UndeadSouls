package souls.undead.inventory;

import java.io.Serializable;

public abstract class Item implements Serializable{
	
	private static final long serialVersionUID = 1L;
	//=====ATTRIBUTES=====//
	private String name;
	private String icon;
	private String desc;
	private int value;
	
	//CONSTRUCTOR
	public Item(String name, String icon, String desc,  int value){
		setName(name);
		setIcon(icon);
		setDesc(desc);
		setValue(value);		
	}
//==========GETTERS=&=SETTERS==========//
	public String getName(){
		return this.name;
	}
	public void setName(String name){
		this.name = name;
	}	
	public String getIcon(){
		return this.icon;
	}
	public void setIcon(String icon){
		this.icon = icon;
	}
	public String getDesc(){
		return this.desc;
	}
	public void setDesc(String desc){
		this.desc = desc;
	}
	public int getValue(){
		return this.value;
	}
	public void setValue(int value){
		this.value = value;
	}
	
}
