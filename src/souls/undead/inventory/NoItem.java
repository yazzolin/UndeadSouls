package souls.undead.inventory;

/**
 * Cetta classe représente les emplacements d'objet vide
 */
public class NoItem extends Item{
	public NoItem(){
		super("None","icon_noitem.png","",0);
	}	
}
