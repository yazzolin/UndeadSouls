package souls.undead.inventory;

public class Weapon extends Item{	
	//=====ATTRIBUTES=====//
	private int damage = 0;
	private double speed = 0;	// vitesse d'attaque de l'arme
	private int element = 0; // Indique s'il s'agit de d�g�ts physiques (0) ou magiques (1)
	//CONSTRUCTOR
	public Weapon(String name, String icon, String desc, int value, int damage, double speed, int element){
		super(name,icon,desc,value);
		setDamage(damage);
		setSpeed(speed);
		setElement(element);
	}	
	//==========GETTERS=&=SETTERS==========//
	public int getDamage(){
		return this.damage;
	}	
	public void setDamage(int damage){
		this.damage = damage;
	}	
	public double getSpeed() {
		return speed;
	}
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	public int getElement() {
		return element;
	}
	public void setElement(int element) {
		this.element = element;
	}
	
}
