package souls.undead.inventory;

public class Armor extends Item{
	//=====ATTRIBUTES=====//
	private int pdef = 0;
	private int mdef = 0;
	//CONSTRUCTOR
	public Armor(String name, String icon, String desc, int value, int pdef, int mdef){
		super(name,icon,desc,value);
		setPdef(pdef);
		setMdef(mdef);
	}	
//=====GETTERS=&=SETTERS=====//
	public int getPdef() {
		return pdef;
	}
	public void setPdef(int pdef) {
		this.pdef = pdef;
	}
	public int getMdef() {
		return mdef;
	}
	public void setMdef(int mdef) {
		this.mdef = mdef;
	}
	
}
