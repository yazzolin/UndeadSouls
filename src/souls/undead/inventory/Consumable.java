package souls.undead.inventory;

public class Consumable extends Item{
	//=====ATTRIBUTES=====//
	private int category; // le type de potion : 0->sant�, 1->mana, 2->stamina
	private int power; // le nombre de points rendu par l'objet 
	private int amount; // le nombre d'exemplaire de l'objet
	private Boolean quickAcces = false; // permet de savoir si l'objet est plac� en raccourci
	
	//CONSTRUCTOR
	public Consumable(String name, String icon, String desc, int value, int amount, int power,  int category){
		super(name,icon,desc,value);
		setAmount(amount);
		setPower(power);
		setCategory(category);
	}
//==========GETTERS=&=SETTERS==========//
	public int getCategory() {
		return category;
	}
	public void setCategory(int category) {
		this.category = category;
	}
	public int getPower() {
		return power;
	}
	public void setPower(int power) {
		this.power = power;
	}
	public Boolean isQuickAcces() {
		return quickAcces;
	}
	public void setQuickAcces(Boolean quickAcces) {
		this.quickAcces = quickAcces;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		if (amount>99)
			amount=99;
		else if (amount<0)
			amount=0;
		this.amount = amount;
	}	
}
