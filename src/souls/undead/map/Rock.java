package souls.undead.map;

public class Rock extends Tile{
	public Rock(int[] position) {
		super(position);
	}

	public String getType() {
		return "Rock";
	}
	
	public String getSprite(){
		return "tile_rock.png";
	}

}
