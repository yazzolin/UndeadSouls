package souls.undead.map;

public class Unknown extends Tile{
	public Unknown(int[] position) {
		super(position);
	}

	public String getType() {
		return "Unkown";
	}
	
	public String getSprite(){
		return "tile_unknown.png";
	}
}
