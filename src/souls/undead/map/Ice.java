package souls.undead.map;

public class Ice extends Tile{
	public Ice(int[] position) {
		super(position);
	}
	public String getType() {
		return "Ice";
	}	
	public String getSprite(){
		return "tile_ice.png";
	}
	
}