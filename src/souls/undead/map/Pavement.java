package souls.undead.map;

public class Pavement extends Tile {
	public Pavement(int[] position) {
		super(position);
	}	
	public String getType() {
		return "Pavement";
	}	
	public String getSprite(){
		return "tile_pavement.png";
	}
}
