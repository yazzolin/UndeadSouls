package souls.undead.map;

public class Grass extends Tile {
	
	public Grass(int[] position) {
		super(position);
	}

	public String getType() {
		return "Grass";
	}
	
	public String getSprite(){
		return "tile_grass.png";
	}
}
