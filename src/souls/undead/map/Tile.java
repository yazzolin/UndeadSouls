package souls.undead.map;

import java.io.Serializable;

public abstract class Tile implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	//=====ATTRIBUTES=====//
	protected boolean walkable=true;
	protected int[]position;
	
	//CONSTRUCTOR	
	public Tile(int[]position){
		setPosition(position);	
	}	
	
//==========GETTERS=&=SETTERS==========//
	public Boolean isWalkable() {
		return walkable;
	}
	public void setWalkable(boolean walkable) {
		this.walkable = walkable;
	}
	public int[] getPosition() {
		return position;
	}
	public void setPosition(int[] position) {
		this.position = position;
	}	
	public abstract String getType();	
	public abstract String getSprite();

} // end of class
