package souls.undead.map;

public class Abyss extends Tile{
	public Abyss(int[] position) {
		super(position);
	}

	public String getType() {
		return "Abyss";
	}
	
	public String getSprite(){
		return "tile_abyss.png";
	}
}

