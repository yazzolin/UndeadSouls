package souls.undead.map;

public class Fence extends Tile{
	public Fence(int[] position) {
		super(position);
		setWalkable(false);
	}
	public String getType(){
		return "Fence";
	}	
	public String getSprite(){
		return "tile_fence.png";
	}
	
}
