package souls.undead.map;

public class Corpse extends Tile {


	public Corpse(int[] position) {
		super(position);
		setWalkable(false);
	}

	public String getType() {
		return "Corpse";
	}

	public String getSprite() {
		return "tile_corpse.png";
	}
}
