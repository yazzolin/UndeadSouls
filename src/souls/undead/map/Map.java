package souls.undead.map;

import java.io.Serializable;
import java.util.*;

import souls.undead.utility.Dice;

public class Map implements Serializable{

	private static final long serialVersionUID = 1L;
	
	//ATTRIBUTES
	public static int DIMENSION = 1;
	public final static String[] SIZES = {"","DEFAULT >","  BIG   >"," GIANT  >"};
	public static int HEIGHT;// hauteur de la carte en nombre de cases
	public static int WIDTH;// largeur de la carte en nombre de cases
	public static int TILESIZE = 48;// longueur d'une case (carr�)
	private ArrayList<int[]> frontier = new ArrayList<int[]>();
	
	//CONSTRUCTOR
	public Map(){}

	
//====GETTERS=&=SETTERS=====//
	public void setDIMENSIONS(int DIMENSIONS){
		if (DIMENSIONS==4)
			DIMENSIONS = 1;
		else if (DIMENSIONS==0)
			DIMENSIONS = 3;
		Map.DIMENSION = DIMENSIONS;
	}
	private void setHEIGHT(int HEIGHT) {
		Map.HEIGHT=HEIGHT;
	}
	private void setWIDTH(int WIDTH) {
		Map.WIDTH = WIDTH;
	}
	private ArrayList<int[]> getFrontier() {
		return frontier;
	}
//=====UTILITY=====//
	
	/**
	 * Transforme les coordoonn�es d'un biome en coordonn�es par rapport � la carte du jeu
	 * @param num : num�ro du biome
	 * @param jend : longueur d'un biome
	 * @param kend : hauteur d'un biome
	 * @return : position du biome
	 */
	public static int[] transformCoords(int num, int jend, int kend){
		int jBig = 0;
		int kBig = 0;
		//System.out.println(num);
		switch(num){
		case 0:
			break;
		case 1: 
			jBig = jend;
			break;
		case 2: 
			jBig = jend*2;
			break;
		case 3: 
			kBig = kend;
			break;
		case 4: 
			jBig = jend;
			kBig = kend;
			break;
		case 5:
			jBig = jend*2;
			kBig = kend;
			break;
		case 6: 
			kBig=kend*2;
			break;
		case 7: 
			jBig = jend;
			kBig = kend*2;
			break;
		case 8:
			jBig = jend*2;
			kBig = kend*2;
			break;
		}
		int[]pos = {jBig,kBig};
		return pos;
	}
//=====MAP=GENERATION=====//
	/**
	 * Cette fonction g�n�re la carte sur laquelle les personnages se d�place
	 * @return Liste de toutes les tuiles qui composent la carte
	 */
	public ArrayList<Tile> generateMap(){
		int len[] = {0,147,258,369}; //MAP SIZES
		setHEIGHT(len[DIMENSION]);
		setWIDTH(len[DIMENSION]);
		//FIRST GENERATE BACKGROUND
		ArrayList<Tile> mapping=new ArrayList<Tile>(WIDTH*HEIGHT);
		for(int i=0; i<WIDTH*HEIGHT;i++){
			if(backConvertPos(i)[0]==0 || backConvertPos(i)[1]==0 || backConvertPos(i)[0]== WIDTH-1 || backConvertPos(i)[1]== HEIGHT-1)
				mapping.add(new Fence(backConvertPos(i)));
			else{
				mapping.add(new Water(backConvertPos(i)));
			}
		}
		//System.out.println("debug array size= "+ mapping.size());
		
		// Creation de 9 biomes avec chacun un type de case diff�rent
		for(int i=0;i<9;i++){
			int[][]biome = new int[(int)(WIDTH/3)][(int)(HEIGHT/3)];//fixed on 9 biomes, must change to make it better
			biome = createBiome(biome,i);
			int jend = biome.length;
			int kend = biome[0].length;
			int[]coords = transformCoords(i,jend,kend);
			int jBig = coords[0];
			int kBig = coords[1];
			//THIRD FOR EACH BIOME GENERATED ADD THE ROOMS THAT CORRESPOND INSIDE THE LIST. 0 = Dirt, 1 = Pavement; 2= Wall; 3 =Grass; 4 =Hotearth; 5 =Ice; 6 =Abyss; 7 =Rock; 8 =Dungeon; 9 =Unknown; 10 =Water; 11 =Corpse
			//System.out.println("jBig= "+jBig+" kBig= "+kBig);
			for(int j=0;j<jend;j++){
				for(int k=0;k<kend;k++){
					int[]pos = {0,0};
					//System.out.println("j= "+j+" k= "+k);
					pos[0] = j+jBig;
					pos[1] = k+kBig;
					//System.out.print(" posx "+pos[0]);
					//System.out.print("posy "+pos[1]);
					//System.out.println("convertPos "+convertPos(pos));
					//System.out.println("debug biome at j k= "+biome[j][k]);
					if(!(mapping.get(convertPos(pos)).getType()=="Fence")){
						if(biome[j][k]==0){
							mapping.set(convertPos(pos), new Dirt(pos));}
						else if(biome[j][k]==1){
							mapping.set(convertPos(pos), new Pavement(pos));
						}else if(biome[j][k]==2){
							mapping.set(convertPos(pos), new Wall(pos));
						}else if(biome[j][k]==3){
							mapping.set(convertPos(pos), new Grass(pos));
						}else if(biome[j][k]==4){
							mapping.set(convertPos(pos), new Hotearth(pos));
						}else if(biome[j][k]==5){
							mapping.set(convertPos(pos), new Ice(pos));
						}else if(biome[j][k]==6){
							mapping.set(convertPos(pos), new Abyss(pos));
						}else if(biome[j][k]==7){
							mapping.set(convertPos(pos), new Rock(pos));
						}else if(biome[j][k]==8){
							mapping.set(convertPos(pos), new Dungeon(pos));
						}else if(biome[j][k]==9){
							mapping.set(convertPos(pos), new Unknown(pos));
						}else if(biome[j][k]==11){
							mapping.set(convertPos(pos), new Corpse(pos));
						}
					}
				}
			}
		}
		return mapping;
	}
	

	
	// Transforme une position dans une matrice en une position dans une ArrayList
	public static int convertPos(int[] pos){
		int position = (pos[0])*WIDTH;
		position = position + pos[1];
		return position;
	}
	
	// Op�ration r�ciproque de la m�thode pr�c�dente
	public static int[] backConvertPos(int position){
		int[]pos = {0,0};
		pos[0] = (int)(position/WIDTH);
		pos[1] = position%WIDTH;
		return pos;
	}
	
	// Permet d'afficher un biome dans la console
	public void render(int[][] biome) {
		//System.out.println("debug: rendering");
			for(int i=0;i<biome.length;i++){
				for(int j=0;j<biome[0].length;j++){
					if(biome[i][j]==0)
						System.out.print("  ");
					else
					System.out.print(biome[i][j]+" ");
				}
				System.out.println("");
			}
		}
	
	// Permet d'afficher la carte dans la console
	public void render(ArrayList<Tile> board) {
		//System.out.println("debug: rendering array");
		for(int i=0; i<board.size();i++){
			String elem=board.get(i).getType();
			//System.out.print("element type " + elem);
			if(elem=="Dirt")
				System.out.print(". ");
			else if(elem=="Pavement")
				System.out.print("_ ");
			else if(elem=="Wall")
				System.out.print("# ");
			else if(elem=="Fence")
				System.out.print("X ");
			else if(elem=="Grass")
				System.out.print(", ");
			else if(elem=="Hotearth")
				System.out.print("@ ");
			else if(elem=="Ice")
				System.out.print("* ");
			else if(elem=="Abyss")
				System.out.print("  ");
			else if(elem=="Rock")
				System.out.print("o ");
			else if(elem=="Dungeon")
				System.out.print("& ");
			else if(elem=="Unknown")
				System.out.print("$ ");
			else if(elem=="Water")
				System.out.print("~ ");
			else if(elem=="Corpse")
				System.out.print("� ");
			if ((i+1)%WIDTH==0)
				System.out.println("");
			}
		}
		//biome generator
	
	private int[][] createBiome(int[][]biome, int i){
		//System.out.println("debug: biome creation");
		biome = addBackground(biome,i);
		biome = addRoom(biome,0,i);
		biome = addObstacles(biome,i);
		//render(biome);
		//biome = makeHallways(biome);
		return biome;
	}
	
	/**
	 * G�n�re un fond diff�rent pour chaque biome
	 * @param biome we are gererating the background og
	 * @param i the number of the biome
	 * @return the biome with the set background
	 */
	private int[][] addBackground(int[][] biome, int i) {
//		System.out.println("size of biome= "+biome.length);
		for(int j =0;j<biome.length;j++){
			for(int k =0;k<biome[0].length;k++){
				int whatTile= Dice.roll(0, 9);
				int probabilityCheck=5;
				int probabilityCheck2=8;
				switch(i){
					case 0:
						if(j<(biome.length/2)){
							probabilityCheck=8;}
						else{
							probabilityCheck=2;}
						if(whatTile<probabilityCheck){
							biome[j][k]=0;
						}else{
							biome[j][k]=5;
						}
					break;
					case 1:
						if(j<(biome.length/2)){
							probabilityCheck=8;}
						else{
							probabilityCheck=2;}
						if(whatTile<probabilityCheck){
							biome[j][k]=5;
						}else{
							biome[j][k]=10;
						}
					break;
					case 2:
						if(j<(biome.length/2)){
							probabilityCheck=8;}
						else{
							probabilityCheck=2;}
						if(whatTile<probabilityCheck){
							biome[j][k]=10;
						}else{
							biome[j][k]=4;
						}
					break;
					case 3:
						if(j<(biome.length/2)){
							probabilityCheck=8;}
						else{
							probabilityCheck=2;}
						if(whatTile<probabilityCheck){
							biome[j][k]=0;
						}else{
							biome[j][k]=3;
						}
					break;
					case 4:
						if(j<biome.length/2){
							if(k<biome.length/2){
								probabilityCheck=7;}
							else{
								probabilityCheck=1;
								probabilityCheck2=8;}
						}else{
							probabilityCheck=1;
							probabilityCheck2=2;}
						if(whatTile<probabilityCheck){
							biome[j][k]=3;
						}else if(whatTile<probabilityCheck2){
							biome[j][k]=8;
						}else{
							biome[j][k]=7;
						}
					break;
					case 5:
						if(j<biome.length/2){
							if(k<biome.length/2){
								probabilityCheck=7;}
							else{
								probabilityCheck=1;
								probabilityCheck2=8;}
						}else{
							probabilityCheck=1;
							probabilityCheck2=2;}
						if(whatTile<probabilityCheck){
							biome[j][k]=4;
						}else if(whatTile<probabilityCheck2){
							biome[j][k]=8;
						}else{
							biome[j][k]=9;
						}
					break;
					case 6:
						if(j<(biome.length/2)){
							probabilityCheck=8;}
						else{
							probabilityCheck=2;}
						if(whatTile<probabilityCheck){
							biome[j][k]=3;
						}
						else{
							biome[j][k]=5;
						}
					break;
					case 7:
						if(j<(biome.length/2)){
							probabilityCheck=8;}
						else{
							probabilityCheck=2;}
						if(whatTile<probabilityCheck){
							biome[j][k]=8;
						}
						else{
							biome[j][k]=6;
						}
					break;
					case 8:
						if(j<(biome.length/2)){
							probabilityCheck=8;}
						else{
							probabilityCheck=2;}
						if(whatTile<probabilityCheck){
							biome[j][k]=6;
						}
						else{
							biome[j][k]=9;
						}
					break;
				}
			}
		}
		return biome;
	}
	/**
	 * Ajoute des salles dans le biome de mani�re r�cursive
	 * @param biome : liste dans laquelle on ajoute les salles
	 * @param count : nombre d'essai de placement, faire varier entre 0 et 1100 pour eviter une erreur de stackOverflow
	 * @param type : num�ro du Biome
	 * @return la liste biome remplie de salles
	 */
	private int[][] addRoom(int[][] biome,int count, int type){
		//System.out.println("debug: addroom count : "+ count);
		if(count<=1100){//TODO BETWEEN 0 and 1100 to prevent stackOverflow
			int size = Dice.roll(3, (int)biome.length/4-1);
			int rectangularity = Dice.roll(0, (int)size/2)*2;
			int width=size;
			int height =size;
			if(Dice.roll(0, 4)<=2){
				width=width+rectangularity;//+type*2;
			}
			else{
				height=height+rectangularity;//+type*2;
			}
			//System.out.println("debug :width = " +width + "height = "+height);
			int x = Dice.roll(2, biome.length-width-4);
			int y=  Dice.roll(2, biome[0].length-height-4);
			//System.out.println("debug :x = " +x + "y = "+y);
			int []rect={x,y,width, height};
			if(overlapse(rect,biome)){
				//System.out.println("debug: add room overlaps");
				addRoom(biome,count+1,type);
			}
			else{
				for(int i=rect[0];i<rect[0]+rect[2];i++){
					for(int j=rect[1];j<rect[1]+rect[3];j++){
						if(i==rect[0] || j==rect[1] || i==rect[0]+rect[2]-1 || j==rect[1]+rect[3]-1){
							biome[i][j]=2;
							if(!((i==rect[0] && j==rect[1]) || ( i==rect[0]&& j==rect[1]+rect[3]-1) || (i==rect[0]+rect[2]-1 && j==rect[1]) || (i==rect[0]+rect[2]-1 && j==rect[1]+rect[3]-1))){
								int[] toAdd = {i,j};
								getFrontier().add(toAdd);
								}
							//else
							//	System.out.println("not in the frontier");
							}
						else{
							biome[i][j]=1;}
					}
				}
				//System.out.println("ONE ROOM CREATED");
				biome = makeDoors(biome);
				biome = addRoom(biome,count+1,type);
			}
		}
		return biome;
	}
	/**
	 * v�rifie si un rectangle ne chevauche pas un rectangle d�j� existant dans la matix
	 * @param rect liste contenant 4 valeurs qui forment un rectangle
	 * @param board matrice dans laquelle on veut mettre un nouveau rectangle
	 * @return true si on essaye de placer le nouveau rectangle sur un emplacement non vide
	 */
	private boolean overlapse(int []rect, int[][]board){
		//System.out.println("debug: overlaps");
		boolean check=false;
		for(int i=rect[0]-1;i<rect[0]+rect[2]+1;i++){
			for(int j=rect[1]-1;j<rect[1]+rect[3]+1;j++){
				if(board[i][j]==2){
					check=true;
				}
			}
		}
		return check;
	}
			//doors generator
	/**
	 * perce des portes dans les salles nouvellement cr�es. La liste fronti�re est n�c�ssaire, elle contient les endroits o� l'on peut percer des portes
	 * @param board matrice contenant les salles
	 * @return matrice avec la derni�re porte perc�e
	 */
	private int[][] makeDoors(int[][]board){
		//System.out.println("frontier = "+getFrontier());
		//int count=0;
		boolean check=false;
		int[]pos = null;
		do{
			if(getFrontier().size()==0)
				break;
			int random = Dice.roll(0, getFrontier().size()-1);
			Collections.shuffle(getFrontier());
			pos = getFrontier().get(random);
			getFrontier().remove(random);
			check = verifyMatching(board,pos);
			//System.out.println("frontierloop = "+count);
			//count++;
		}
		while(!check);
		if(check)
			board[pos[0]][pos[1]]=1;
		getFrontier().clear();
		return board;
	}
	/**
	 * v�rifie si une case est accessible ou non
	 * @param board matrice contenant des salles
	 * @param pos position ou l'on veut v�rifier la correspondance
	 * @return true si le nombre de cases non vides est �gal a 2
	 */
	private boolean verifyMatching(int[][]board, int[]pos){
		boolean check=false;
		short count=0;
		if((board[pos[0]-1][pos[1]]==2 && board[pos[0]-1][pos[1]]==2))
			count++;
		if((board[pos[0]+1][pos[1]]==2 && board[pos[0]+1][pos[1]]==2))
			count++;
		if((board[pos[0]][pos[1]-1]==2 && board[pos[0]][pos[1]-1]==2))
			count++;
		if((board[pos[0]][pos[1]+1]==2 && board[pos[0]][pos[1]+1]==2))
			count++;
		if(count==2)
			check=true;
		return check;
	}
	//
	private int[][] addObstacles(int[][] biome, int i) {
		for(int j=1;j<biome.length-1;j++){
			for(int k=1;k<biome[0].length-1;k++){
				int obschance = Dice.roll(0, 50); // une chance sur 50 qu'une case soit un obstacle
				if(obschance<1 && (biome[j+1][k]!=2&&biome[j+1][k]!=1) && (biome[j-1][k]!=2&&biome[j-1][k]!=1) && (biome[j][k+1]!=2&&biome[j][k+1]!=1) && (biome[j][k-1]!=2&&biome[j][k-1]!=1)){
					biome[j][k] = 11;
				}
			}
		}
		return biome;
	}
	
}
