package souls.undead.map;

public class Hotearth extends Tile{
	public Hotearth(int[] position) {
		super(position);
	}

	public String getType() {
		return "Hotearth";
	}
	
	public String getSprite(){
		return "tile_hotearth.png";
	}
}
