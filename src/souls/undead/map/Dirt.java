package souls.undead.map;

public class Dirt extends Tile {
	public Dirt(int[] position) {
		super(position);
	}
	public String getType() {
		return "Dirt";
	}	
	public String getSprite(){
		return "tile_dirt.png";
	}
	
}
