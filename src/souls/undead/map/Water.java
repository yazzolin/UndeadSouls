package souls.undead.map;

public class Water extends Tile{
	public Water(int[] position) {
		super(position);
	}

	public String getType() {
		return "Water";
	}
	
	public String getSprite(){
		return "tile_water.png";
	}
}
