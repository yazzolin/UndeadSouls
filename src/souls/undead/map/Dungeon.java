
package souls.undead.map;

public class Dungeon extends Tile{
	public Dungeon(int[] position) {
		super(position);
	}

	public String getType() {
		return "Dungeon";
	}
	
	public String getSprite(){
		return "tile_dungeon.png";
	}

}
