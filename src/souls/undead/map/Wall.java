package souls.undead.map;

public class Wall extends Tile {
	public Wall(int[] position) {
		super(position);
		setWalkable(false);
	}
	public String getType(){
		return "Wall";
	}	
	public String getSprite(){
		return "tile_wall.png";
	}
}
