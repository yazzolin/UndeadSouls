package souls.undead.utility;

import java.util.Random;

public class Dice {
	/**
	 * Un d� dont le minimum et maximum sont param�trables.
	 * @param a la valeur minimum renvoy�e
	 * @param b la valeur maximum renvoy�e
	 * @return un nombre al�atoire entre a et b compris
	 */
	public static int roll(int a, int b){
		Random ran = new Random();
		int r = ran.nextInt(b-a+1)+a;
		return r;
	}
}
