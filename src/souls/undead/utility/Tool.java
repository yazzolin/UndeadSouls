package souls.undead.utility;

public class Tool {
	public static int[] directionToVector(int direction){
		int x = 0;
		int y = 0;
		switch(direction){
		case 0:
			x+=1;
			break;
		case 1:
			x-=1;
			break;
		case 2:
			y-=1;
			break;
		case 3:
			y+=1;
			break;
		}
		int pos[] = {x,y};
		return pos;
	}
	
	public static int euclideanDistance(int[] a, int[] b){
		int dist = ((int) Math.sqrt( Math.pow(a[0]-b[0],2) +Math.pow(a[1]-b[1],2)));
		return dist;		
	}
}
