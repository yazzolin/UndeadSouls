package souls.undead.utility;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import souls.undead.mvc.Data;

/**
 * Permet de conserver un journal de notifications qui 
 * apparaissent tout au long du jeu. Ces messages sont affich�s
 * sur l'affichage t�te haute, et enregistr� dans un fichier texte.
 */
public class Gamelog {
	//ATTRIBUTE
	private final static String filename = Data.DIRECTORY+"log.txt"; // le nom du fichier contenant toutes les notifications
	
	// Cr�er un fichier "log.txt" vide.
	public static void init(){
        try {
            FileWriter fw = new FileWriter(filename);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.close();
        }catch(IOException e){}
	}

	public static void addText(String string){
        try {
            FileWriter fw = new FileWriter(filename,true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(string);
            bw.newLine();
            bw.close();
        }catch(IOException e){
        	System.out.println("io Exception Gamelog");
        }
	}	
	
	public static int lineCount(){
		int count=0;
		try{
			BufferedReader br = new BufferedReader(new FileReader(filename));
			while (br.readLine()!=null){
				count++;
			}
			br.close();
		}catch(IOException e){}
		return count;
	}
}
