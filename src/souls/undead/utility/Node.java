package souls.undead.utility;

public class Node {
	//ATTRIBUTES
	private int cost = 100000; // Nombre de d�placements n�cessaires pour arriver � destination
	private Node previous;
	private boolean discovered = false;
	private int[] position;
	private int bondValue = 1; // D�termine le co�t du d�placement vers le node. Un mur aurait un co�t infini par exemple (tr�s �lev�)
	
	//CONSTRUCTOR
	public Node(int x, int y){
		int[]pos = {x,y};
		this.position = pos;
	}
	public Node(int x, int y,int bondValue){
		int[]pos = {x,y};
		this.position = pos;
		setBondValue(bondValue);
	}
	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}
	public boolean isDiscovered() {
		return discovered;
	}
	public void setDiscovered(boolean discovered) {
		this.discovered = discovered;
	}
	public int[] getPosition() {
		return position;
	}
	public void setPosition(int[] position) {
		this.position = position;
	}
	public void setPrevious(Node previous) {
		this.previous = previous;
	}
	public Node getPrevious() {
		return previous;
	}
	public int getBondValue() {
		return bondValue;
	}
	public void setBondValue(int bondValue) {
		this.bondValue = bondValue;
	}

}
