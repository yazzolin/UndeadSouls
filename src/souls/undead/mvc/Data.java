package souls.undead.mvc;

import java.io.Serializable;
import java.util.ArrayList;

import souls.undead.map.Map;
import souls.undead.map.Tile;
import souls.undead.characters.Bonfire;
import souls.undead.characters.Character;

public class Data implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	//ATTRIBUTES
	private Map map;
	private ArrayList<Tile> mapdata = new ArrayList<Tile>(); // Contient toutes les cases de la carte
	private ArrayList<Character> charList = new ArrayList<Character>(); // Contient tous les personnages existants
	private ArrayList<Bonfire> bonfires = new ArrayList<Bonfire>(9); // Contient la liste des points de teleportation
	
	//STATIC
	public final static String[][] CLASSLIST = {{"Warrior","Figther specialized in armed close quarter combat."},
												{"Mage","Magic user casting various spells from a distance."},
												{"Saiyan","Powerful warrior with exceptional strength and speed."},
												{"Pokemon","Short and chubby yellow mouse that uses electric-type attack."}
	};
	public final static int NUMBEROFCLASS = CLASSLIST.length;	
	public static int BASICATKCOST = 4; // Co�t en stamina d'une attaque de base
	public static int EVASIONCOST = 2; // C�ut en stamine d'une esquive
	public static int SKILL1COST = 4; //c�ut en magie d'un skill 1
	public static int SKILL2COST = 5; //c�ut en magie d'un skill 2
	public static int DIFFICULTY = 2;
	public static int MOVETYPE = 0; // d�termine la fa�on dont le personnage bouge � l'aide des fl�ches directionnelles
	public static int STARTPOINT = 0; // d�termine si le joueur d�marre � une position al�atoire ou non
	public final static String[] DIFFICULTIES = {"","NOSKILL >","DEFAULT >","GIT GUD >"};
	public final static String[] STATUSES = {"","status_burnt.png"};
	public final static String[] MOVETYPES = {"DEFAULT >","SIMPLER >"};
	public final static String[] STARTPOINTS = {"DEFAULT >","BONFIRE >"};
	public final static String DIRECTORY = "data/"; // dossier o� sont sauv�s les fichiers cr��s par le jeu
	//public final static String[] SKILLSSPRITES = {"","hurtbox_whirlwind.png","","",""};
	
	//CONSTRUCTOR
	public Data(){}

//=====GETTERS=&=SETTERS=====//
	
	public static void setBASICATKCOST(int bASICATKCOST) {
		BASICATKCOST = bASICATKCOST;
	}
	public static void setEVASIONCOST(int eVASIONCOST) {
		EVASIONCOST = eVASIONCOST;
	}
	public static void setDIFFICULTY(int dIFFICULTY) {
		if (dIFFICULTY==4)
			dIFFICULTY = 1;
		else if (dIFFICULTY==0)
			dIFFICULTY = 3;
		DIFFICULTY = dIFFICULTY;
	}
	public static void setMOVETYPE() {
		MOVETYPE = 1-MOVETYPE;
	}	
	public static void setSTARTPOINT() {
		STARTPOINT = 1-STARTPOINT;
	}

	//NON-STATIC
	public Map getMap() {
		return map;
	}
	public void setMap(Map map) {
		this.map = map;
	}
	public ArrayList<Tile> getMapdata() {
		return mapdata;
	}
	public void setMapdata(ArrayList<Tile> mapdata) {
		this.mapdata = mapdata;
	}
	
	public ArrayList<Character> getCharList() {
		return charList;
	}

	public void setCharList(ArrayList<Character> charList) {
		this.charList = charList;
	}

	public ArrayList<Bonfire> getBonfires() {
		return bonfires;
	}

	public void setBonfires(ArrayList<Bonfire> bonfires) {
		this.bonfires = bonfires;
	}

	public Character getPlayer(){
		Character player = null;
		try{
			player = charList.get(0);
		}catch(IndexOutOfBoundsException e){}
		return player;
	}
	public Character getNpc(String name){
		Character npc = null;
		for (Character c : charList){
			if (c.getName().equals(name))
				npc = c;
		}
		return npc; 
	}

}
