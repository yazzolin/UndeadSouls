package souls.undead.mvc;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import souls.undead.actions.AlternateMove;
import souls.undead.characters.Armourer;
import souls.undead.characters.Artorias;
import souls.undead.characters.BlackKnight;
import souls.undead.characters.Blacksmith;
import souls.undead.characters.Bonfire;
import souls.undead.characters.Character;
import souls.undead.characters.Chest;
import souls.undead.characters.Enemy;
import souls.undead.characters.Ghost;
import souls.undead.characters.Mage;
import souls.undead.characters.Neutral;
import souls.undead.characters.Npc;
import souls.undead.characters.Pc;
import souls.undead.characters.Pokemon;
import souls.undead.characters.Saiyan;
import souls.undead.characters.Skeleton;
import souls.undead.characters.Solaire;
import souls.undead.characters.Sorceress;
import souls.undead.characters.Warrior;
import souls.undead.inventory.Armor;
import souls.undead.inventory.Consumable;
import souls.undead.inventory.Inventory;
import souls.undead.inventory.Item;
import souls.undead.inventory.Weapon;
import souls.undead.map.Map;
import souls.undead.map.Tile;
import souls.undead.threads.SpRegen;
import souls.undead.utility.Dice;
import souls.undead.utility.Gamelog;
import souls.undead.utility.Tool;

public class Game{
	
	//ATTRIBUTES
	private Data data; // L'ensemble des donn�es serialisables
	private Window window;
	private ArrayList<Tile> tileFov = new ArrayList<Tile>(); // Contient les cases visibles par le joueur
	private ArrayList<Character> charFov = new ArrayList<Character>(); // Contient les personnages visibles par le joueur
	private int[] invSelect = {240,336}; // La position du curseur de l'inventaire
	private int menuSelect = 475; // La positon du curseur dans certains menus
	private int invSelectNum = 0; // L'index de l'objet surlign� par le curseur
	private int logLine = 544; // Ordonn�e � laquelle une notification est affich�e
	//private boolean saveData = false;
			
	//CONSTRUCTOR
	public Game(Window window){
		this.window = window;		
	}
//==========GETTERS=&=SETTERS==========//
	
	//=====DISPLAY=RELATED=====//
	public Data getData(){
		return data;
	}
	public int getScreen() {
		return window.getScreen();
	}
	public void setScreen(int screen) {
		window.setScreen(screen);
		repaint();
	}
	//le jeu attend que tous les threads actifs se terminent avant de proc�der
	public void deathScreen(){
		setScreen(Window.YOUDIEDSCREEN);
		for (Character c : data.getCharList()){
			if (c.getThread()!=null){
				if (!c.getThread().getState().equals(Thread.State.TERMINATED)){
					c.getThread().interrupt();
				}
			}
		}
		
	}
	public int[] getInvSelect(){
		return invSelect;
	}	
	public int getInvSelectNum(){
		return invSelectNum;
	}
	public int getMenuSelect(){
		return menuSelect;
	}
	public void moveInvSelect(int dx, int y){
		int newx = invSelect[0]+dx;
		int newy = y;
		if (newx<240)
			newx=240;
		else if (newx>720)
			newx=720;
		if (y==0)
			newy=invSelect[1];
		invSelect[0] = newx;
		invSelect[1] = newy;
	}	
	public void setMenuSelect(int menuSelect) {
		this.menuSelect = menuSelect;
	}	
	public void moveMenuSelect(int dy){
		int newy = getMenuSelect() + dy;		
		if (getScreen()!=Window.SHOPSCREEN && getScreen()!=Window.TELEPORTSCREEN){
			if (newy==450)//menu
				newy=500;
			else if (newy==525)//menu
				newy=475;
			else if (newy==120)//class
				newy=120+25*Data.NUMBEROFCLASS;
			else if (newy==145+25*Data.NUMBEROFCLASS)//class
				newy=145;
		}else if(getScreen()==Window.TELEPORTSCREEN){
			if (newy==145-25)
				newy=145+8*25;
			else if (newy==145+9*25)
				newy=145;
		}
		else{
			if (newy==145-25)
				newy=145+(((Neutral)(data.getPlayer().interact())).getShop().size()-1)*25;
			else if (newy==145+((Neutral)(data.getPlayer().interact())).getShop().size()*25)
				newy=145;
		}
		menuSelect = newy;
		repaint();
	}	
	public int getLogLine() {
		return logLine;
	}
	public void setLogLine(int logLine) {
		if (logLine>583)
			logLine=544;
		this.logLine = logLine;
	}
	public void changeSettings(int dx){
		switch(menuSelect){
		case 145:
			Data.setDIFFICULTY(Data.DIFFICULTY+dx);
			break;
		case 170:
			data.getMap().setDIMENSIONS(Map.DIMENSION+dx);
			break;
		case 195:
			Data.setMOVETYPE();
			break;
		case 220:
			Data.setSTARTPOINT();
			break;
		}
		repaint();
	}
	//=====CHARARCTERS=RELATED=====//
	public Character getNpc(String name){
		Character npc = null;
		for (Character c : data.getCharList()){
			if (c.getName().equals(name))
				npc = c;
		}
		return npc; 
	}

	public void createPlayer(){
		switch(getMenuSelect()){
		case 145:
			addPlayer(new Warrior());
			break;
		case 170:
			addPlayer(new Mage());
			break;
		case 195:
			addPlayer(new Saiyan());
			break;
		case 220:
			addPlayer(new Pokemon());
			break;
		}
		if (Data.MOVETYPE==0)
			data.getPlayer().setMove(new AlternateMove());
		data.getPlayer().setGame(this);
		data.getPlayer().setThread( new Thread(new SpRegen(data.getPlayer())) );
	}	
	public void addPlayer(Character player){
		player.setStartingPosition(data.getMapdata(),data.getCharList());
		player.setGame(this);
		data.getCharList().add(player);
		if(player.getClass().isAssignableFrom(Pc.class.getClass())){
			data.getCharList().set(0, player);
			}
	}	
	public void addRewardDrop(Character drop){
		drop.setGame(this);
		data.getCharList().add(drop);
		refreshView();
	}
	public void addPlayer(Character player, String tileType){
		player.setSpecialStartingPosition(data.getMapdata(), data.getCharList(), tileType);
		player.setGame(this);
		data.getCharList().add(player);
		if(player.getClass().isAssignableFrom(Pc.class.getClass())){
			data.getCharList().set(0, player);}
	}

	public void removeNpc(Character npc){
		if (!npc.getName().equals("Chest")){
			npc.soulsReward(data.getPlayer());
			data.getCharList().remove(npc);
			((Npc) npc).rewardDropChance();
		}
		else{
			if (!data.getPlayer().getInventory().isFull())
				data.getCharList().remove(npc);
			npc.itemReward(data.getPlayer());
		}
		repaint();
	}	
	
	private void generateEnemies(){
		int n;
		for (n=0;n<(int)(Math.pow(Map.HEIGHT, 2)/(100-Data.DIFFICULTY*10));n++){
			int enemyType = Dice.roll(1, 5);
			if (enemyType!=1)
				addPlayer(new Skeleton());
			else
				addPlayer(new Ghost(),"Pavement");	
		}
		addPlayer(new Solaire());
	}
	
	private void generateNeutrals(){
		addPlayer(new Blacksmith(),"Hotearth");
		addPlayer(new Armourer(),"Grass");
		addPlayer(new Sorceress(),"Water");
		addPlayer(new Artorias(),"Abyss");
		addPlayer(new BlackKnight(),"Dungeon");
	}
	
	private void generateChests(){
		for (int n=0;n<(int)(Map.HEIGHT)/(Data.DIFFICULTY*2);n++)
			addPlayer(new Chest(),"Pavement");
	}
	
	private void generateBonfire(){
		for(int i=0;i<9;i++){
			Character bonfire = new Bonfire(i);
			addPlayer(bonfire);
			data.getBonfires().add((Bonfire)bonfire);
		}
	}

	private void generateNpcs(){
		generateEnemies();
		generateNeutrals();
		generateChests();
		generateBonfire();
	}
	
	//==========PLAYER=ACTIONS==========//
	public void playerAction(String action,int direction,int dx,int dy){
		Character player = data.getPlayer();
		if(!player.isInAction()){
			if(action == "movement"){
				player.move(dx, dy);
				}
			}
	}
	
	public void playerAction(String action){
		Character player = data.getPlayer();
		if(!player.isInAction()){			
			switch(action){
			case "attack":
				if(player.getSp()>=Data.BASICATKCOST){
					player.attack();
				}else if(player.getSp()<Data.BASICATKCOST){
					Gamelog.addText(player.getName()+" is exhausted");
				}
				break;
			case "skill1": case "skill2":
				if(action.equals("skill1") && player.getMp()>=player.getSkill1().getMpCost() && player.getSp()>=player.getSkill1().getSpCost()){
					player.useSkill(0);
				}
				else if (action.equals("skill2") && player.getMp()>=player.getSkill2().getMpCost() && player.getSp()>=player.getSkill2().getSpCost()){
					player.useSkill(1);
				}
				else{
					if (player.getMp()<player.getSkill1().getMpCost())
						Gamelog.addText(player.getName()+" needs more mana");
					if (player.getSp()<player.getSkill1().getSpCost())
						Gamelog.addText(player.getName()+" is exhausted");
				}
				break;
			case "dodge":
				if (player.getSp()>=Data.EVASIONCOST){
					player.dodge();
				}
				else
					Gamelog.addText(player.getName()+" is exhausted");
				break;
			case "use0":
				player.useQuickItem(0);;	
				break;
			case "use1":
				player.useQuickItem(1);
				break;
			case "use2":
				player.useQuickItem(2);
				break;
			case "use3":
				player.useQuickItem(3);
				break;
			case "use4":
				player.useQuickItem(4);
				break;
			}
			player.staminaRegen();
		}
	}
	
	public void playerInteraction(){
		Character npc = data.getPlayer().interact();
		if (npc!=null){
			if (npc.getName().equals("Smithy") || npc.getName().equals("Maughlin") || npc.getName().equals("Sorceress")){
				Gamelog.addText("Welcome to my shop !");
				setMenuSelect(145);
				setScreen(Window.SHOPSCREEN);
			}else if (npc.getName().equals("Artorias")){
				Gamelog.addText("...");
				npc.itemReward(data.getPlayer());
			}else if (npc.getName().equals("Black Knight")){
				Gamelog.addText("...");
				npc.itemReward(data.getPlayer());
			}else if (npc.getName().equals("Chest")){
				removeNpc(npc);
			}else if (npc.getName().equals("Bonfire")){
				save();
				data.getPlayer().reachBonfire();
				setMenuSelect(145);
				setScreen(Window.TELEPORTSCREEN);
			}
		}else{
			Gamelog.addText("There is nothing to do !");
		}
		repaint();
	}
	
	private void updateThreads(){
		for (Character c : charFov){
			if(c.getRole().equals("Enemy"))
				((Enemy)c).updateThread();
		}
	}
	
	//=====MAP=RELATED=====//
	/**
	 *  R�cup�re les cases et ennemis que le joueur voit � l'�cran, sachant que le joueur est toujours
	 *  affich� au centre de l'�cran, sauf lorsqu'il se trouve dans les coins de la carte.
	 */
	private void updateFov(){
		Character c = data.getPlayer();		
		ArrayList<Tile> tileFov = new ArrayList<Tile>();
		ArrayList<Character> charFov = new ArrayList<Character>();
		int xCenter=c.getPosition()[0];
		int yCenter=c.getPosition()[1];				  
		if(xCenter<=(Window.WIDTH-1)/2)
			xCenter=(Window.WIDTH-1)/2;//=10
		else if(xCenter>=Map.WIDTH-((Window.WIDTH-1)/2+1))
			xCenter=Map.WIDTH-((Window.WIDTH-1)/2+1);//=11
		if(yCenter<=(Window.HEIGHT-2)/2)
			yCenter=(Window.HEIGHT-2)/2;//=5
		else if(yCenter>=Map.HEIGHT-((Window.HEIGHT-2)/2+1))
			yCenter=Map.HEIGHT-((Window.HEIGHT-2)/2+1);//=6
		int[]topLeft={xCenter-(Window.WIDTH-1)/2, yCenter-(Window.HEIGHT-2)/2};
		int[]bottomRight={xCenter+(Window.WIDTH-1)/2, yCenter+(Window.HEIGHT-2)/2};

		for(int i=Map.convertPos(topLeft);i<=Map.convertPos(bottomRight)/*Map.convertPos(bottomRight)*/;i++){
			Tile element=data.getMapdata().get(i);
			if((Math.abs(element.getPosition()[0]-xCenter)<=(Window.WIDTH-1)/2) && (Math.abs(element.getPosition()[1]-yCenter)<=(Window.HEIGHT-2)/2)){
				tileFov.add(element);
			}
		}
		for(Character foe : data.getCharList()){
			if((Math.abs(foe.getPosition()[0]-xCenter)<=(Window.WIDTH-1)/2) && (Math.abs(foe.getPosition()[1]-yCenter)<=(Window.HEIGHT-2)/2)){
				charFov.add(foe);	
			}
		}

		this.tileFov = tileFov;
		this.charFov = charFov;
	}

	public ArrayList<Tile> getTileFov(){
		return this.tileFov;
	}
	
	public ArrayList<Character> getCharFov(){
		return this.charFov;
	}
	
	//=====ITEMS=RELATED=====//
	public void interactWithItem(){
		try{
			Item item = (data.getPlayer()).getInventory().getPouch().get(invSelectNum);
			if (item instanceof Weapon || item instanceof Armor){
				(data.getPlayer()).getInventory().equipItem(invSelectNum,(data.getPlayer()).getInventory().getPouch().get(invSelectNum));
				data.getPlayer().updateStats();
			}
			else if (item instanceof Consumable){
				data.getPlayer().useItem(item);
			}
		}catch (IndexOutOfBoundsException e ){}
	}
	
	public void buyItem(){
		int index = (menuSelect-145)/25;
		Item item = ((Neutral) data.getPlayer().interact()).getShop().get(index);
		((Pc)data.getPlayer()).buyItem(item);
		repaint();
	}
	
	public void setItemShortcut(int slot){
		try{
			data.getPlayer().getInventory().setShortcut(slot, data.getPlayer().getInventory().getPouch().get(getInvSelectNum()));
		}catch (IndexOutOfBoundsException e){}
	}

//==========COLLISION==========//
	/**
	 * V�rifie si le joueur entre ou non en collision avec un obstacle.
	 * Si c'est le cas, la position du joueur ne change pas, sinon sa
	 * position est mise � jour normalement.
	 * @param dx : D�placement horizontale
	 * @param dy : D�placement verticale
	 * @return renvoie s'il y a collision ou non
	 */
	public Boolean checkCollision(Character character, int dx,int dy){
		Boolean ok = true;
		int[] newPos = {character.getPosition()[0] + dx, character.getPosition()[1] + dy};
		ok = checkWallCollision(ok, newPos[0], newPos[1]);
		ok = checkCharCollision(ok, newPos[0], newPos[1]);
		return ok;
	}
	// Collision avec les murs
	private Boolean checkWallCollision(Boolean ok, int x, int y){
		int i;
		for (i=0;i<tileFov.size();i++){
			Tile tile = tileFov.get(i);
			if (tile.getPosition()[0]==x && tile.getPosition()[1]==y){
				ok = tile.isWalkable();
			}
		}		
		return ok;
	}
	// Collision avec d'autres personnages
	public Boolean checkCharCollision(boolean ok, int x, int y){ 
		for (int i=0;i<charFov.size();i++){
			Character character = charFov.get(i);
			if (character.getPosition()[0]==x && character.getPosition()[1]==y){
				ok = false;
			}
		}	
		return ok;
	}	
	
	// Pour le pathfinding des ennemis
	public Boolean checkCharCollision(int x, int y){ 
		Boolean ok= false;
		for (int i=1;i<data.getCharList().size();i++){
			Character character = data.getCharList().get(i);
			if (character.getPosition()[0]==x && character.getPosition()[1]==y){
				ok = true;
			}
		}	
//		System.out.println(ok);
		return ok;
	}		
	
	/**
	 * Fonction appel�e lorsqu'une attaque de base est lanc�e. Elle v�rifie si le joueur touche un monstre.
	 * Si la port�e du joueur est strictement sup�rieure � 1, sa port�e d'attaque est repr�sentable sous
	 * la forme d'une croix. Si plusieurs ennemis se trouvent sur la m�me ligne et dans la port�e d'attaque,
	 * seul le plus proche sera attaqu�.
	 */
	public ArrayList<Character> checkAttackCollision(int x, int y, int direction, int range, boolean aoe){ //CHECK COLLISION, CROSS SHAPED
		ArrayList<Character> targets = new ArrayList<Character>();
		for (int i=0;i<charFov.size();i++){
			Character enemy = charFov.get(i);
			switch(direction){
			case 0:
				if (enemy.getPosition()[1]==y && enemy.getPosition()[0]>x && enemy.getPosition()[0]<=x+range)
					targets.add(enemy);
				break;
			case 1:
				if (enemy.getPosition()[1]==y && enemy.getPosition()[0]<x && enemy.getPosition()[0]>=x-range)
					targets.add(enemy);
				break;
			case 2:
				if (enemy.getPosition()[0]==x && enemy.getPosition()[1]<y && enemy.getPosition()[1]>=y-range)
					targets.add(enemy);
				break;
			case 3:
				if (enemy.getPosition()[0]==x && enemy.getPosition()[1]>y && enemy.getPosition()[1]<=y+range)
					targets.add(enemy);
				break;
			}
		}
		if (range>1 && targets.size()>1 && !aoe){
			Character foe = findNearest(x,y,direction,targets);
			targets.clear();
			targets.add(foe);				
		}
		if (targets.size()==0)
			targets.add(null);
		
		return targets;
	}
	
	// Cherche toutes les cibles adjacentes au joueur
	public ArrayList<Character> checkCrossAttackCollision(int x, int y, int direction, int range){ //CHECK COLLISION, CROSS SHAPED, MULTIPLE HITS
		 ArrayList<Character> targets = new ArrayList<Character>();
		for (int i=0;i<charFov.size();i++){
			Character enemy = charFov.get(i);
			if (enemy.getPosition()[1]==y && enemy.getPosition()[0]>x && enemy.getPosition()[0]<=x+range)
				targets.add(enemy);
			else if (enemy.getPosition()[1]==y && enemy.getPosition()[0]<x && enemy.getPosition()[0]>=x-range)
				targets.add(enemy);
			else if (enemy.getPosition()[0]==x && enemy.getPosition()[1]<y && enemy.getPosition()[1]>=y-range)
				targets.add(enemy);
			else if (enemy.getPosition()[0]==x && enemy.getPosition()[1]>y && enemy.getPosition()[1]<=y+range)
				targets.add(enemy);
		}
		return targets;	
	}
	
	// Cherche toutes les cibles comprises dans un rayon �gal � la port�e de l'attaque
	public ArrayList<Character> checkSplashAttackCollision(int x, int y, int direction, int range) {
		ArrayList<Character> targets = new ArrayList<Character>();
		int pos[] = {Tool.directionToVector(direction)[0]*range,Tool.directionToVector(direction)[1]*range};
		pos[0] += x;
		pos[1] += y;
		for (int i=0;i<charFov.size();i++){
			Character foe = charFov.get(i);
			if (Tool.euclideanDistance( pos,foe.getPosition() )<range)
				targets.add(foe);
		}
		return targets;
	}
	
	// Trouve quelle est la cible le plus proche du joueur, lors d'une attaque � distance
	// o� plusieurs ennemis se trouvent dans la port�e d'attaque.
	private Character findNearest(int x, int y, int direction, ArrayList<Character> alist){
		Character nearest = alist.get(0);
		for (Character foe : alist){
			if (direction<=1){
				if (Math.abs(x-foe.getPosition()[0])<Math.abs(x-nearest.getPosition()[0]))
					nearest = foe;
			}else{
				if (Math.abs(y-foe.getPosition()[1])<Math.abs(y-nearest.getPosition()[1]))
					nearest = foe;
			}
		}			
		return nearest;
	}
//===============GAME=LOGIC===============//
		public void init(){				
			//updateSaveStatus();
			data = new Data();
			data.setMap(new Map());
			data.getBonfires().clear();
			invSelect[0] = 240;
			invSelect[1] = 336;
			menuSelect = 475;
			invSelectNum = 0;
			logLine = 544;
			window.getBoard().setGame(this);
			setScreen(Window.TITLESCREEN);
			Gamelog.init();	
		}
		
		public void start(){
			data.setMapdata(data.getMap().generateMap());
			createPlayer();
			generateNpcs();
			refreshInv();
			refreshView();
			bonfireStart();
			setScreen(Window.GAMESCREEN);
			save();
		}
		
		public void bonfireStart(){
			if (Data.STARTPOINT==1){
				data.getPlayer().setPosition(data.getBonfires().get(0).getPosition()[0]+1, data.getBonfires().get(0).getPosition()[1]);
				refreshView();
			}
		}
		
		public void checkpoint() {
			Gamelog.init();
			load();
			data.getPlayer().respawn(this);
			refreshInv();
			refreshView();
			setScreen(Window.GAMESCREEN);
		}
		// Mets � jour l'affichage ainsi que le champ de vision du joueur.
		public void refreshView(){
			if (data.getPlayer()!=null)
				updateFov();
			updateThreads();
			repaint();
			//System.out.println(data.getPlayer().getPosition()[0]+"/"+data.getPlayer().getPosition()[1]);
		}
		// Mets � jour l'affichage de l'inventaire.
		public void refreshInv(){
			invSelectNum = ((invSelect[0]-240)/Map.TILESIZE + Inventory.LINELENGTH * (invSelect[1]-336)/Map.TILESIZE);
			//System.out.println(invSelectNum);
			//updateItemBonus();
			repaint();
		}
		// Mets � jour l'affichage.
		public void repaint(){
			window.getBoard().repaint();
		}
		private void clearEnemies(){
			ArrayList<Character> backup = new ArrayList<Character>();
			backup.add(data.getPlayer());
			for (Character neutral : data.getCharList()){
				if (neutral.getRole().equals("Object") || neutral.getRole().equals("Neutral")){
					if (neutral.getRole().equals("Neutral"))
						neutral.respawn(this);
					backup.add(neutral);
				}
			}
			data.getCharList().clear();
			data.setCharList(backup);
			generateEnemies();
		}
//=====SERIALIZATION=====//	
		public void save(){
			try{
				ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(Data.DIRECTORY+"save.data"));
				output.writeObject(data);
				output.close();
			}catch(IOException e){
				e.printStackTrace();
			}
		}

		private void load(){
			try{
				ObjectInputStream input = new ObjectInputStream(new FileInputStream(Data.DIRECTORY+"save.data"));
				this.data = (Data) input.readObject();
				input.close();
			}catch(IOException | ClassNotFoundException e){
				e.printStackTrace();
			}	
			clearEnemies();
		}
		
//		public boolean getSaveData(){
//			return this.saveData;
//		}
		
//		public void updateSaveStatus(){
//			try{
//				ObjectInputStream input = new ObjectInputStream(new FileInputStream("savedata/save.data"));
//				saveData = true;
//				input.close();
//			}catch(IOException e){
//				saveData = false;
//			}
//		}
		
//=====DEBUGGING=====//		
		public void test(){
			//data.getPlayer().teleport();
			data.getPlayer().setSouls(99999);
			System.out.println("your position : "+data.getPlayer().getPosition()[0]+"/"+data.getPlayer().getPosition()[1]);
			Character npc = data.getNpc("Smithy");
			System.out.println("npc location  : "+npc.getPosition()[0]+"/"+npc.getPosition()[1]);
		}
	
}// end of class
