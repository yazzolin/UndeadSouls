package souls.undead.mvc;

import java.awt.Dimension;
import javax.swing.JFrame;
import souls.undead.map.Map;

public class Window extends JFrame{
	//ATTRIBUTES
	private Board board;
	private int screen = 0;
	final static int HEIGHT = 12;
	final static int WIDTH  = 21;
	final static int TITLESCREEN = 0;
	final static int MENUSCREEN = 1;
	final static int CLASSSCREEN = 2;
	final static int SETTINGSSCREEN = 3;
	final static int GAMESCREEN = 4;	
	final static int STATSSCREEN = 6;
	final static int INVSCREEN = 8;
	final static int SHOPSCREEN = 10;
	final static int TELEPORTSCREEN =11;
	public final static int VICTORYSCREEN = 555;
	final static int YOUDIEDSCREEN = 666;
	
	//CONSTRUCTOR
	public Window(Board board){
		this.board = board;
		board.setPreferredSize(new Dimension(WIDTH*Map.TILESIZE,HEIGHT*Map.TILESIZE)); // 
		this.getContentPane().add(board);
		this.setTitle("Undead Souls v1.0");
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
	}
//==========GETTERS=&=SETTERS==========//
	public Board getBoard() {
		return board;
	}
	public int getScreen() {
		return screen;
	}
	public void setScreen(int screen) {
		this.screen = screen;
	}	
}