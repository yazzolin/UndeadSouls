package souls.undead.mvc;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import souls.undead.characters.Pc;
import souls.undead.utility.Gamelog;

/**
 * Le contr�leur; intercepte toutes les entr�es claviers et change  
 * les informations contenues dans les mod�les, selon la touche appuy�e.
 */
public class Keyboard implements KeyListener{
	
	//ATTRIBUTES
	private Game game;
	
	//CONSTRUCTOR
	public Keyboard(Game game){
		this.game = game;
	}
	
	public void keyPressed(KeyEvent event){
		int key = event.getKeyCode();
		switch (game.getScreen()){
		case (Window.GAMESCREEN):
			playerActions(key);
			if (key!=KeyEvent.VK_ESCAPE)
				game.refreshView();
			break;
		case (Window.TITLESCREEN):
			if (key==KeyEvent.VK_ENTER)
				game.setScreen(Window.MENUSCREEN);
				game.setMenuSelect(475);
			break;
		case (Window.MENUSCREEN):
			if (key==KeyEvent.VK_ENTER)
				menuChoice();
			menuSelect(key);
			break;
		case (Window.SETTINGSSCREEN):
			settingsScreen(key);
			menuSelect(key);
			break;
		case (Window.CLASSSCREEN):
			if (key==KeyEvent.VK_ENTER && (game.getMenuSelect()==145 || game.getMenuSelect()==170 || game.getMenuSelect()==195 || game.getMenuSelect()==220)){
				game.start();}
			else if (key==KeyEvent.VK_BACK_SPACE){
				game.setScreen(Window.MENUSCREEN);
				game.setMenuSelect(475);}		
			menuSelect(key);
			break;
		
		case (Window.STATSSCREEN):
			statsScreen(key);
			break;
		
		case (Window.INVSCREEN):
			invScreen(key);
			break;
		case (Window.SHOPSCREEN):
			shopScreen(key);
			menuSelect(key);
			break;
		case (Window.VICTORYSCREEN):
			if (key==KeyEvent.VK_ENTER)
				game.init();
			break;
		case (Window.YOUDIEDSCREEN):
			if (key==KeyEvent.VK_ENTER){
				game.checkpoint();
				game.setScreen(Window.GAMESCREEN);}
			break;
		case (Window.TELEPORTSCREEN):
			teleportScreen(key);
			menuSelect(key);
		}
	}
//=====SUBSWITCH=====//
	private void playerActions(int key){
		switch(key)
		{
		case (KeyEvent.VK_ESCAPE):
			game.init();
			break;
		case (KeyEvent.VK_X)://DEBUGGING
			game.test();
			break;
		case (KeyEvent.VK_RIGHT):
			game.playerAction("movement", 0, 1, 0);
			break;
		case (KeyEvent.VK_LEFT):
			game.playerAction("movement", 1, -1, 0);
			break;
		case (KeyEvent.VK_UP):
			game.playerAction("movement", 2, 0, -1);
			break;
		case (KeyEvent.VK_DOWN):
			game.playerAction("movement", 3, 0, 1);
			break;
		case (KeyEvent.VK_SPACE):
			game.playerAction("dodge");;
			break;
		case (KeyEvent.VK_1):
			game.playerAction("use0");
			break;
		case (KeyEvent.VK_2):
			game.playerAction("use1");
			break;
		case (KeyEvent.VK_3):
			game.playerAction("use2");
			break;
		case (KeyEvent.VK_4):
			game.playerAction("use3");
			break;
		case (KeyEvent.VK_5):
			game.playerAction("use4");
			break;
		case (KeyEvent.VK_A):
			game.playerAction("attack");
			break;
		case (KeyEvent.VK_Z):
			game.playerAction("skill1");
			break;
		case (KeyEvent.VK_S):
			game.playerAction("skill2");
			break;
		case (KeyEvent.VK_E):
			game.playerInteraction();
			break;
		case (KeyEvent.VK_I):
			game.setScreen(Window.INVSCREEN);
			break;
		case (KeyEvent.VK_P):
			game.setScreen(Window.STATSSCREEN);
			break;
		case (KeyEvent.VK_C):
			Gamelog.init();
			break;
		}
	}
	
	private void invScreen(int key){
		switch (key){
		case(KeyEvent.VK_E):
			game.interactWithItem();
			break;
		case (KeyEvent.VK_R):
			try{
				game.getData().getPlayer().getInventory().removeShortcut(game.getData().getPlayer().getInventory().getPouch().get(game.getInvSelectNum()));
			}catch(IndexOutOfBoundsException e){}
			break;
		case (KeyEvent.VK_T):
			try{
				game.getData().getPlayer().getInventory().throwItem(game.getData().getPlayer().getInventory().getPouch().get(game.getInvSelectNum()));
			}catch (IndexOutOfBoundsException e){}
			break;
		case (KeyEvent.VK_I):
			game.setScreen(Window.GAMESCREEN);
			break;
		case (KeyEvent.VK_BACK_SPACE):
			game.setScreen(Window.GAMESCREEN);
			break;
		case (KeyEvent.VK_P):
			game.setScreen(Window.STATSSCREEN);
			break;
		case (KeyEvent.VK_C):
			Gamelog.init();
		case (KeyEvent.VK_1):
			game.setItemShortcut(0);
			break;
		case (KeyEvent.VK_2):
			game.setItemShortcut(1);
			break;
		case (KeyEvent.VK_3):
			game.setItemShortcut(2);
			break;
		case (KeyEvent.VK_4):
			game.setItemShortcut(3);
			break;
		case (KeyEvent.VK_5):
			game.setItemShortcut(4);
			break;
		// D�place le curseur de l'inventaire d'une case dans la direction concern�e
		case (KeyEvent.VK_RIGHT):
			game.moveInvSelect(48, 0);
			break;
		case (KeyEvent.VK_LEFT):
			game.moveInvSelect(-48, 0);
			break;
		case (KeyEvent.VK_UP):
			game.moveInvSelect(0, 336);
			break;
		case (KeyEvent.VK_DOWN):
			game.moveInvSelect(0, 384);
			break;
		}			
		game.refreshInv();		
	}
	
	private void statsScreen(int key){
		switch (key){
		case (KeyEvent.VK_U):
			game.getData().getPlayer().levelUp();
			break;
		case (KeyEvent.VK_I):
			game.setScreen(Window.INVSCREEN);
			break;
		case (KeyEvent.VK_P):
			game.setScreen(Window.GAMESCREEN);
			break;
		case (KeyEvent.VK_BACK_SPACE):
			game.setScreen(Window.GAMESCREEN);
			break;
		case (KeyEvent.VK_C):
			Gamelog.init();
			game.repaint();
			break;
		case (KeyEvent.VK_1):
			((Pc) game.getData().getPlayer()).levelUpStat(0);
			break;
		case (KeyEvent.VK_2):
			((Pc) game.getData().getPlayer()).levelUpStat(1);
			break;
		case (KeyEvent.VK_3):
			((Pc) game.getData().getPlayer()).levelUpStat(2);
			break;
		case (KeyEvent.VK_4):
			((Pc) game.getData().getPlayer()).levelUpStat(3);
			break;
		case (KeyEvent.VK_5):
			((Pc) game.getData().getPlayer()).levelUpStat(4);
			break;
		case (KeyEvent.VK_6):
			((Pc) game.getData().getPlayer()).levelUpStat(5);
			break;
		}		
	}
	
	private void menuSelect(int key){
		//int k = event.getKeyCode();
		switch (key){
		case (KeyEvent.VK_UP):
			game.moveMenuSelect(-25);
			break;
		case (KeyEvent.VK_DOWN):
			game.moveMenuSelect(25);
			break;
		}
	}
	
	private void menuChoice(){
		switch(game.getMenuSelect()){
		case 475:
			game.setScreen(Window.CLASSSCREEN);
			game.setMenuSelect(145);
			break;
//		case 500:
//			if (game.getSaveData()){
//				game.checkpoint();
//			}
//			break;
		case 500:
			game.setScreen(Window.SETTINGSSCREEN);
			game.setMenuSelect(145);
			
			break;
		}
	} 
	
	private void settingsScreen(int key){
		switch (key){
		case (KeyEvent.VK_BACK_SPACE): case (KeyEvent.VK_ENTER):
			game.setScreen(Window.MENUSCREEN);
			game.setMenuSelect(475);
			break;
		case (KeyEvent.VK_RIGHT):
			game.changeSettings(1);
			break;
		case (KeyEvent.VK_LEFT):
			game.changeSettings(-1);
			break;
		}
	}
	
	private void shopScreen(int key){
		switch(key){
		case (KeyEvent.VK_BACK_SPACE):
			game.setScreen(Window.GAMESCREEN);
			Gamelog.addText("Come back again !");
			break;
		case (KeyEvent.VK_E):
			game.buyItem();
			break;
		}
	}

	private void teleportScreen(int key){
		switch (key){
		case (KeyEvent.VK_BACK_SPACE):
			game.setScreen(Window.GAMESCREEN);
			break;
		case (KeyEvent.VK_E):
			game.getData().getPlayer().teleport();
			game.setScreen(Window.GAMESCREEN);
			break;
		}
	}
	public void keyTyped(KeyEvent event){}
	public void keyReleased(KeyEvent event){} 
	
//	case (KeyEvent.VK_ADD){ //DEBUGGING
//	int n;
//	for (n=0;n<10;n++){
//		game.addPlayer(new Skeleton());
//	}
//}
}
