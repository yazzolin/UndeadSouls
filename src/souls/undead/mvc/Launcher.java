package souls.undead.mvc;

public class Launcher {
	public static void main(String[] args) {
		Window window = new Window(new Board());
		Game game = new Game(window);
		Keyboard keyboard = new Keyboard(game);
		window.setVisible(true);		
		window.addKeyListener(keyboard);
		game.init();
	}
}
