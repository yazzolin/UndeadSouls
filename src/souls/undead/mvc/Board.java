package souls.undead.mvc;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

import souls.undead.actions.FinalFlash;
import souls.undead.characters.Bonfire;
import souls.undead.characters.Character;
import souls.undead.characters.Neutral;
import souls.undead.characters.Pc;
import souls.undead.inventory.Armor;
import souls.undead.inventory.Consumable;
import souls.undead.inventory.Inventory;
import souls.undead.inventory.Item;
import souls.undead.inventory.Weapon;
import souls.undead.map.Map;
import souls.undead.map.Tile;
import souls.undead.utility.Gamelog;
import souls.undead.utility.Tool;

/**
 * La vue. Elle est contient les m�thodes responsables d'afficher 
 * les diff�rents �l�ments graphiques du jeu, comme les personnages.
 * Elle peut acc�der � Game pour savoir ce qu'elle doit dessiner,
 * mais cet acc�s est uniquement en lecture.
 */
public class Board extends JPanel{	
	//ATTRIBUTES
	private Game game;

	//SHORTCUTS
	private final Color BLACK = Color.BLACK;
	private final Color WHITE = Color.WHITE;
	private final int PLAIN = Font.PLAIN;
	private final int BOLD = Font.BOLD;
	
	//CONSTRUCTOR
	public Board(){}
	
	//=====DISPLAY=====//	
	public void paintComponent(Graphics g){	
		Graphics2D g2 = (Graphics2D) g;
		switch (game.getScreen()){
			case Window.TITLESCREEN:
				drawTitle(g2);
				break;
			case Window.MENUSCREEN:
				drawMenu(g2);
				break;
			case Window.CLASSSCREEN:
				drawCharSelect(g2);
				break;
			case Window.GAMESCREEN:
				drawMap(g2);
				drawEnemies(g2);
				drawPlayer(g2);
				break;
			case Window.SETTINGSSCREEN:
				drawSettings(g2);
				break;
			case Window.STATSSCREEN:
				drawPlayerStatus(g2);
				break;
			case Window.INVSCREEN:
				drawInventory(g2);
				break;				
			case Window.SHOPSCREEN:
				drawShop(g2);
				break;
			case Window.TELEPORTSCREEN:
				drawTeleport(g2);
				break;
			case Window.VICTORYSCREEN:
				picture(g,"victory.png",0,0);
				break;
			case Window.YOUDIEDSCREEN:
				picture(g,"preparetodie.png",0,0);
				break;
		}
		if (game.getScreen()>=Window.GAMESCREEN && game.getScreen()<15)
			drawHud(g2);
	}
//==========GETTERS=&=SETTERS==========//
	public void setGame(Game game){
		this.game = game;
	}
//=====HANDY=FUNCTIONS=====//
	/**
	 * Fonction affichant l'image de son choix � la position voulue.
	 * Toutes les images doivent provenir du dossier "/img".
	 * @param g
	 * @param filename le nom de l'image � afficher (extension comprise)
	 * @param x l'abscisse de l'image, par rapport � la fen�tre
	 * @param y l'ordonn�e de l'image, par rapport � la fen�tre
	 */
	private void picture(Graphics g, String filename, int x, int y){
	    try{
	        Image img = ImageIO.read(new File("img/"+filename));
	        g.drawImage(img, x, y, this);
	    }catch (IOException e) {} 	
	}
	/**
	 * Affiche le text choisi � la position voulue.
	 * @param g
	 * @param text le texte � afficher
	 * @param x l'abscisse du texte
	 * @param y l'ordonn�e du texte
	 * @param size la taille de la police du texte
	 */
	private void text(Graphics g, String text, int x, int y, int size){
		g.setFont(new Font("Courier", Font.PLAIN, size));
		g.setColor(Color.WHITE);
		g.drawString(text, x, y);
	}
	//Overlead de la m�thode pr�c�dente
	private void text(Graphics g, String text, int x, int y, int style, int size, Color color){
		g.setFont(new Font("Courier", style, size));
		g.setColor(color);
		g.drawString(text, x, y);
	}

//===============DRAW=FUNCTIONS==============//	
	private void drawTitle(Graphics g){
		picture(g,"title.png",0,0);
		text(g,"PRESS ENTER",390,522,BOLD,35,WHITE);
	}		
	private void drawMenu(Graphics g){
		picture(g,"title.png",0,0);
		picture(g,"cursor.png",390,game.getMenuSelect());
		text(g,"NEW GAME",435,495,BOLD,30,WHITE);
//		if (game.getSaveData())
//			text(g,"CONTINUE",435,520,BOLD,30,WHITE);
//		else
		//text(g,"   NA   ",435,520,BOLD,30,WHITE);
		text(g,"SETTINGS",435,520,BOLD,30,WHITE);
	}
	
	private void drawSettings(Graphics g){
		picture(g,"settings.png",0,0);
		picture(g,"cursor.png",52,game.getMenuSelect());
		text(g,"DIFFICULTY  :  < "+Data.DIFFICULTIES[Data.DIFFICULTY],95,160,24);
		text(g,"WORLD SIZE  :  < "+Map.SIZES[Map.DIMENSION],95,185,24);
		text(g,"MOVMT TYPE  :  < "+Data.MOVETYPES[Data.MOVETYPE],95,210,24);
		text(g,"STARTPOINT  :  < "+Data.STARTPOINTS[Data.STARTPOINT],95,235,24);
	}

	private void drawCharSelect(Graphics g){
		picture(g,"role_select.png",0,0);
		picture(g,"cursor.png",52,game.getMenuSelect());
		for (int i=0;i<Data.NUMBEROFCLASS;i++){
			String name = Data.CLASSLIST[i][0];
			text(g,name,95,160+i*25,24);
		}
		text(g,Data.CLASSLIST[(game.getMenuSelect()-145)/25][1],115,490,18);
	}
	/**
	 * Dessine le personnage incarn� par le joueur. Par d�faut, il se trouve au centre de l'�cran.
	 * Cependant, lorsqu'il se trouve aux coins de la carte, celle-ci ne se d�place plus. Dans ce
	 * cas de figure, la cam�ra reste fixe.
	 */
	private void drawPlayer(Graphics g){
		Character player = game.getData().getPlayer();
		int[] newPos = {10,5};
		if(player.getPosition()[0]<(Window.WIDTH-1)/2 || player.getPosition()[0]>=Map.WIDTH-((Window.WIDTH-1)/2+1))
			newPos[0]=player.getPosition()[0];
		if(player.getPosition()[0]>=Map.WIDTH-((Window.WIDTH-1)/2+1))
			newPos[0] -= Map.WIDTH-Window.WIDTH;
		if(player.getPosition()[1]<(Window.HEIGHT-2)/2 || player.getPosition()[1]>=Map.HEIGHT-((Window.HEIGHT-2)/2+1))
			newPos[1] =player.getPosition()[1];
		if( player.getPosition()[1]>=Map.HEIGHT-((Window.HEIGHT-2)/2+1))
			newPos[1] -= Map.HEIGHT-Window.HEIGHT+1;
		picture(g, player.getSprite(), newPos[0]*Map.TILESIZE, newPos[1]*Map.TILESIZE);
		picture(g, Data.STATUSES[player.getStatus()], newPos[0]*Map.TILESIZE, newPos[1]*Map.TILESIZE);
		drawEffect(g,newPos[0]*Map.TILESIZE, newPos[1]*Map.TILESIZE,player.getDirection());
	}	
	
	private void drawEffect(Graphics g, int x, int y, int direction){
		switch(game.getData().getPlayer().getSkillNum()){
		case 1:
			picture(g,"hurtbox_whirlwind.png", x-48, y-48);
			break;
		case 2:
			picture(g,"hurtbox_fireball.png",x+Tool.directionToVector(direction)[0]*144-96,y+Tool.directionToVector(direction)[1]*144-96);
			break;
		case 3:
			drawBeam(g,x,y,direction);
			break;
		}
		
	}
	
	private void drawBeam(Graphics g, int x, int y, int direction){
		switch(direction){
		case 0:
			x+=Map.TILESIZE;
			y-=Map.TILESIZE;
			break;
		case 1:
			x-=Map.TILESIZE*19;
			y-=Map.TILESIZE;
			break;
		case 2:
			x-=Map.TILESIZE;
			y-=Map.TILESIZE*10;
			break;
		case 3:
			x-=Map.TILESIZE;
			y+=Map.TILESIZE;
			break;
		}
		picture(g,FinalFlash.sprites[direction],x,y);
	}
	
	private void drawInventory(Graphics g){
		int i;
		try{
			picture(g, "inventory.png", 191, 48);
			for (i=0;i<3;i++){
				Item item = game.getData().getPlayer().getInventory().getEquipment().get(i);
				picture(g,item.getIcon(),215,100+i*50);//240
				text(g,item.getName(),270,130+i*50,BOLD,18,WHITE);//306
				text(g,item.getDesc(),540,130+i*50,18);

			}
			int[] pos={242,338};
			for (i=0;i<game.getData().getPlayer().getInventory().getPouch().size();i++){
				Item item = game.getData().getPlayer().getInventory().getPouch().get(i);
				if (i<11){		
					picture(g,item.getIcon(),pos[0]+i*Map.TILESIZE,pos[1]);
					drawShortcutIcon(g,item,pos[0]+i*Map.TILESIZE+27,pos[1]+28);
				}
				else{
					picture(g,item.getIcon(),pos[0]+i*Map.TILESIZE-(Inventory.LINELENGTH*Map.TILESIZE),pos[1]+Map.TILESIZE);
					drawShortcutIcon(g,item,pos[0]+i*Map.TILESIZE-(Inventory.LINELENGTH*Map.TILESIZE)+27,pos[1]+Map.TILESIZE+28);
				}	

				//if (item instanceof Consumable)
					//text(g, Integer.toString(((Consumable) item).getAmount()) ,pos[0]+i*Map.TILESIZE+30,pos[1]+42,BOLD,25,BLACK);
			}
		}catch(NullPointerException e){}
		picture(g,"cursor_inv.png",game.getInvSelect()[0],game.getInvSelect()[1]);
	}
	// Affiche une icone indiquant qu'un objet � �t� plac� en raccourci, si n�cessaire.
	private void drawShortcutIcon(Graphics g, Item item, int x, int y){
		try{
			if (item.getClass().equals(Consumable.class) && ((Consumable) item).isQuickAcces())
				picture(g,"icon_shortcut.png",x,y);
		}catch(ClassCastException e){}
	}
	
	private void drawPlayerStatus(Graphics g){
		Character player = game.getData().getPlayer();
		picture(g, "status.png", 191, 48);
		text(g,player.getName()+" - "+player.getRole(),220,124,18);
		text(g,"Soul Level "+player.getLevel(),220,145,18);
		int up = player.getLevel()*2;//(int)(player.getLevel() + player.getLevel()*(player.getLevel()/1.5)+2);
			text(g,"Next level : "+up+" souls required",440,140,18);
		if (((Pc) player).getUpPoints()>0)
			text(g,"Up points  : "+((Pc) player).getUpPoints()  ,440,124,PLAIN,18,Color.GREEN);
		text(g,"Health  : "+player.getHp()+"/"+player.getMhp(),255,180,18);
		text(g,"Mana    : "+player.getMp()+"/"+player.getMmp(),255,200,18);
		text(g,"Stamina : "+player.getSp()+"/"+player.getMsp(),255,220,18);
		text(g,"Vitality     : "+player.getVitality(),255,262,18);
		text(g,"Endurance    : "+player.getEndurance(),255,282,18);
		text(g,"Strength     : "+player.getStrength(),255,302,18);
		text(g,"Agility      : "+(player.getAgility()+((Pc)player).getBonus()[3]),255,322,18);
		text(g,"Power        : "+player.getPower(),255,343,18);
		text(g,"Intelligence : "+player.getIntelligence(),255,363,18);
		text(g,"Range : "+player.getRange(),550,220,18);		
		NumberFormat dot = NumberFormat.getNumberInstance(Locale.ENGLISH);
		DecimalFormat asFormat = new DecimalFormat("#.0");
		DecimalFormat msFormat = new DecimalFormat("#.0");
		asFormat = (DecimalFormat)dot;
		msFormat = (DecimalFormat)dot;		
		text(g,"Atk speed   : "+asFormat.format(player.getAttackSpeed()),550,262,18);
		text(g,"Mvt speed   : "+msFormat.format(player.getMovementSpeed()),550,282,18);
		text(g,"Phy Attack  : "+(player.getStrength()+((Pc)player).getBonus()[11]),550,302,18);
		text(g,"Mag Attack  : "+(player.getPower()+((Pc)player).getBonus()[12]),550,322,18);
		text(g,"Phy Defense : "+(player.getPdef()+((Pc)player).getBonus()[13]),550,343,18);
		text(g,"Mag Defense : "+(player.getMdef()+((Pc)player).getBonus()[14]),550,363,18);
	}
	private void drawHud(Graphics g){
		Character player = game.getData().getPlayer();
		int souls = player.getSouls();
		picture(g,"hud.png",0,(Window.HEIGHT-1)*Map.TILESIZE);
		text(g,"HP : "+player.getHp()+"/"+player.getMhp(),366,544,BOLD,17,new Color(200,0,0));
		text(g,"MP : "+player.getMp()+"/"+player.getMmp(),366,558,BOLD,17,new Color(0,128,255));
		text(g,"SP : "+player.getSp()+"/"+player.getMsp(),366,572,BOLD,17,new Color(0,200,0));
		if (souls<10){
			text(g,Integer.toString(souls),266,566,BOLD,25,BLACK);
		}else if (souls>=10 && souls<100){
			text(g,Integer.toString(souls),259,566,BOLD,25,BLACK);
		}else{
			text(g,Integer.toString(souls),250,566,BOLD,25,BLACK);
		}
		drawLog(g);
		drawShortcuts(g);
		if (player.isInAction())
			picture(g,"icon.png",570,542);
		if (player.isHurt()){
			picture(g,"hurtscreen.png",0,0);
		}else if (player.isInvincible())
			picture(g,"iframescreen.png",0,0);
		
	}
	private void drawShortcuts(Graphics g){
		int n;
		try {
			for (n=0;n<game.getData().getPlayer().getInventory().getShortcuts().size();n++){
				Item item = game.getData().getPlayer().getInventory().getShortcuts().get(n);
				if (item!=null)
					picture(g,item.getIcon(),2+48*n,530);
			}
		}catch(NullPointerException e){}
	}
	
	private void drawLog(Graphics g){
		String line = null;
        int lineCount = Gamelog.lineCount();
        game.setLogLine(544);
        int i;
        try {
            FileReader fileReader = new FileReader(Data.DIRECTORY+"log.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            if (lineCount<=3){
            	for(i=0;i<lineCount;i++){            		
            		line = bufferedReader.readLine();
            		text(g,line,760,game.getLogLine(),BOLD,12,WHITE);
            		game.setLogLine(game.getLogLine()+13);
            	}	
            }else{
            	for(i=1;i!=lineCount+1;i++){
            		line = bufferedReader.readLine();
            		if (i>lineCount-3){
            			text(g,line,760,game.getLogLine(),BOLD,12,WHITE);
            			game.setLogLine(game.getLogLine()+13);
            		}
            	}
            }
            bufferedReader.close();         
        }
        catch(FileNotFoundException e){}
        catch(IOException e) {}
    }

	// Affiche la partie de la carte se trouvant dans le champ de vision du joueur.
	private void drawMap(Graphics g){
		for(Tile element:game.getTileFov()){
			int[]newPos = {element.getPosition()[0] - game.getTileFov().get(0).getPosition()[0],element.getPosition()[1] - game.getTileFov().get(0).getPosition()[1]};
			picture(g,element.getSprite(),newPos[0]*Map.TILESIZE ,newPos[1]*Map.TILESIZE);
		}
	}	
	// Affiche les monstres se trouvant dans le champ de vision du joueur.
	private void drawEnemies(Graphics g) {
		for(int i=1;i<game.getCharFov().size();i++){
			Character c = game.getCharFov().get(i);
			int[]newPos = {c.getPosition()[0] - game.getTileFov().get(0).getPosition()[0],c.getPosition()[1] - game.getTileFov().get(0).getPosition()[1]};
			picture(g,c.getSprite(),newPos[0]*Map.TILESIZE ,newPos[1]*Map.TILESIZE);
			picture(g,Data.STATUSES[c.getStatus()],newPos[0]*Map.TILESIZE ,newPos[1]*Map.TILESIZE);
			if (c.isHurt()){
				picture(g,"hurtbox.png",newPos[0]*Map.TILESIZE ,newPos[1]*Map.TILESIZE);
			}
				
//			if (c.getThread()!=null)
//				text(g,c.getThread().getName(),newPos[0]*Map.TILESIZE ,newPos[1]*Map.TILESIZE,BOLD,12,Color.RED);
		}
	}
	// Affiche l'�cran d'achat du PNJ concern�
	private void drawShop(Graphics g){
		Neutral npc = (Neutral) game.getData().getPlayer().interact();
		picture(g,npc.getShopSprite(),191,48);
		picture(g,"cursor.png",210,game.getMenuSelect());
//		NumberFormat dot = NumberFormat.getNumberInstance(Locale.ENGLISH);
//		DecimalFormat asFormat = new DecimalFormat("#.0");
//		asFormat = (DecimalFormat)dot;
		Item item = null;
		for (int i=0;i<npc.getShop().size();i++){
			if (npc.getName().equals("Smithy"))
				item = (Weapon)npc.getShop().get(i);
			else if (npc.getName().equals("Maughlin"))
				item = (Armor)npc.getShop().get(i);
			else if (npc.getName().equals("Sorceress"))
				item = (Weapon)npc.getShop().get(i);
			text(g,item.getName(),255,160+i*25,18);
			text(g,item.getDesc(),455,160+i*25,18);
			//text(g,asFormat.format(item.getSpeed()),650,160+i*25,18);
			text(g,Integer.toString(item.getValue()),735,160+i*25,18);
		}
	}
	private void drawTeleport(Graphics g){
		Neutral npc =(Neutral) game.getData().getPlayer().interact();
		picture(g,npc.getShopSprite(),191,48);
		picture(g,"cursor.png",210,game.getMenuSelect());
		for (int i=0;i<9;i++){
			if (((Bonfire) npc).getLocation()==i)
				text(g,game.getData().getBonfires().get(i).getName(i),255,160+i*25,BOLD,18,WHITE);
			else
				text(g,game.getData().getBonfires().get(i).getName(i),255,160+i*25,18);	
		}
	}
	
}// end of class
