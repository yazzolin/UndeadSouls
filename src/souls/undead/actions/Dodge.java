package souls.undead.actions;

import souls.undead.characters.Character;

public interface Dodge {
	public void avoidDamage(Character c);
}
