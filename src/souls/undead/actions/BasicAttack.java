package souls.undead.actions;

import java.util.ArrayList;

import souls.undead.characters.Character;
import souls.undead.mvc.Data;

public class BasicAttack extends AttackTemplate implements Attack {
	
	private static final long serialVersionUID = 1L;

	public BasicAttack(){
		super(Data.BASICATKCOST,0,0,1);
	}
	
	public void damageDealt(Character attacker){
		int damage = attacker.getStrength();	
		if (attacker.getElement()==0)
			damage = attacker.getStrength()+attacker.getBonus()[11];
		else
			damage = attacker.getPower()+attacker.getBonus()[12];		
		ArrayList<Character> targets = atkTarget(attacker,damage,0,false,0);
	}
	
}
