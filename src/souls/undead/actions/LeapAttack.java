package souls.undead.actions;

import souls.undead.characters.Character;
import java.util.ArrayList;
//<<<<<<< Updated upstream:src/souls/undead/actions/LeapAttack.java
//=======
//
//import souls.undead.characters.Character;
//>>>>>>> Stashed changes:src/souls/undead/actions/LeapAttack.java
import souls.undead.mvc.Data;

public class LeapAttack extends AttackTemplate implements Skill {

	private static final long serialVersionUID = 1L;
	
	public LeapAttack(){
		super(5,3,1.5,2);
	}
	
	public void useSkill(Character attacker) {
		int damage = 2*attacker.getStrength()+attacker.getBonus()[11];
		damage += attacker.getAgility();	
		ArrayList<Character> foe = atkTarget(attacker,damage,0,false,0);
		if(foe.get(0)!=null && attacker.getStatus()==0){
			if(foe.get(0).getPosition()[0]==attacker.getPosition()[0] && foe.get(0).getPosition()[1]==attacker.getPosition()[1]+2)
				attacker.move(0, 1);
			else if(foe.get(0).getPosition()[0]==attacker.getPosition()[0] && foe.get(0).getPosition()[1]==attacker.getPosition()[1]-2)
				attacker.move(0, -1);
			else if(foe.get(0).getPosition()[0]==attacker.getPosition()[0]+2 && foe.get(0).getPosition()[1]==attacker.getPosition()[1])
				attacker.move(1, 0);
			else if(foe.get(0).getPosition()[0]==attacker.getPosition()[0]-2 && foe.get(0).getPosition()[1]==attacker.getPosition()[1])
				attacker.move(-1, 0);
		}
	}
}
