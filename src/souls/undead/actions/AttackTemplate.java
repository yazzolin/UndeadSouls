package souls.undead.actions;

import java.io.Serializable;
import java.util.ArrayList;

import souls.undead.characters.Bonfire;
import souls.undead.characters.Character;
import souls.undead.characters.Chest;
import souls.undead.characters.Enemy;
import souls.undead.characters.Neutral;
import souls.undead.characters.Pc;
import souls.undead.threads.AttackThread;
import souls.undead.utility.Gamelog;

public abstract class AttackTemplate implements Serializable{
	
	private static final long serialVersionUID = 1L;

	protected int spCost = 0;
	protected int mpCost = 0;
	protected double speed = 0;
	protected int range = 1;
	
	public AttackTemplate(int spCost, int mpCost, double speed, int range){
		this.spCost = spCost;
		this.mpCost = mpCost;
		this.speed = speed;
		this.range = range;
	}
	
	protected ArrayList<Character> atkTarget(Character attacker,int damage,int hitbox,boolean aoe,int effect){
		attacker.setSp(attacker.getSp()-spCost);
		attacker.setMp(attacker.getMp()-mpCost);
		ArrayList<Character> targets = new ArrayList<Character>();
		switch(hitbox){
		case 0:
			targets = attacker.getGame().checkAttackCollision(attacker.getPosition()[0], attacker.getPosition()[1], attacker.getDirection(), range, aoe);
			break;
		case 1:
			targets = attacker.getGame().checkCrossAttackCollision(attacker.getPosition()[0], attacker.getPosition()[1], attacker.getDirection(), range);
			break;
		case 2:
			targets = attacker.getGame().checkSplashAttackCollision(attacker.getPosition()[0], attacker.getPosition()[1], attacker.getDirection(), range);
			break;
		}
		
		((new Thread(new AttackThread(attacker, effect, speed)))).start();
		
		for(Character foe:targets){
			try{
				if (attacker.getRole().equals("Enemy") && foe.getRole().equals("Enemy") || foe.getRole().equals("Neutral") && attacker.getRole().equals("Enemy") || foe.getRole().equals("Object"))
					foe = null;
			}catch(NullPointerException e){
				foe = null;
			}
			if(foe!=null){
				foe.hurt(damage,attacker.getElement());
			}
			else{
				if (attacker instanceof Pc)
					Gamelog.addText("There is no target");
			}	
		}
		return targets;
	}
	
	public int getSpCost() {
		return spCost;
	}
	public void setSpCost(int spCost) {
		this.spCost = spCost;
	}
	public int getMpCost() {
		return mpCost;
	}
	public void setMpCost(int mpCost) {
		this.mpCost = mpCost;
	}
	public double getSpeed() {
		return speed;
	}
	public void setSpeed(double speed) {
		if (speed<=0)
			speed = 0.1;
		this.speed = speed;
	}
	public int getRange() {
		return range;
	}
	public void setRange(int range) {
		this.range = range;
	}
	
}