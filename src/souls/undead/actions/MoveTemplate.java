package souls.undead.actions;

import souls.undead.map.Map;

import java.io.Serializable;

import souls.undead.characters.Character;
import souls.undead.threads.MoveThread;

public abstract class MoveTemplate  implements Serializable{
	
	private static final long serialVersionUID = 1L;

	protected void basicMove(Character character, int dx, int dy){
		int[] newPos = {character.getPosition()[0]+dx,character.getPosition()[1]+dy};
		if (newPos[0]>=0 && newPos[0]<(Map.WIDTH) && newPos[1]>=0 && newPos[1]<(Map.HEIGHT) && character.getGame().checkCollision(character ,dx, dy))
			character.setPosition(newPos[0],newPos[1]);
		Thread movement = new Thread(new MoveThread(character));
		movement.start();
	}
}
