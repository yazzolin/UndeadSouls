package souls.undead.actions;

import souls.undead.characters.Character;

public interface Skill {
	public void useSkill(Character character);
	public int getSpCost();
	public int getMpCost();
}
