package souls.undead.actions;

import souls.undead.characters.Character;

public interface Move {
	
	public void move(Character character, int dx, int dy);

}
