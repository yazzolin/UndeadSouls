package souls.undead.actions;

import souls.undead.characters.Character;

public class AlternateMove extends MoveTemplate implements Move{
	
	private static final long serialVersionUID = 1L;
	
	public void move(Character character, int dx, int dy) {
		if (dx==1 && character.getDirection()==0
				|| dx==-1 && character.getDirection()==1
				|| dy==-1 && character.getDirection()==2
				|| dy==1 && character.getDirection()==3){
			basicMove(character, dx ,dy);
		}else{
			if (dx==1)
				character.setDirection(0);
			else if (dx==-1)
				character.setDirection(1);
			else if (dy==-1)
				character.setDirection(2);
			else if (dy==1)
				character.setDirection(3);
		}
	}
}