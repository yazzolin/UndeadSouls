package souls.undead.actions;

import souls.undead.characters.Character;

public interface Hurt {
	public void damageTaken(Character defender,int amount, int element);
}
