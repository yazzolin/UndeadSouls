package souls.undead.actions;

import souls.undead.characters.Character;
import souls.undead.characters.Pc;
import souls.undead.mvc.Data;
import souls.undead.threads.BurnThread;
import souls.undead.utility.Dice;

public class BurningAttack extends AttackTemplate implements Attack{

	private static final long serialVersionUID = 1L;

	public BurningAttack(){
		super(Data.BASICATKCOST,0,0,3);
	}
	
	public void damageDealt(Character attacker){
		int damage = attacker.getPower()+((Pc)attacker).getBonus()[12];		
		Character foe = atkTarget(attacker,damage,0,false,0).get(0);
		int chance = Dice.roll(1, 3);
		if(foe!=null && attacker.getStatus()==0 && chance==1)
			(new Thread(new BurnThread(foe,attacker.getPower()))).start();
	}

}
