package souls.undead.actions;

import souls.undead.characters.Character;
import souls.undead.characters.Npc;
import souls.undead.characters.Pc;
import souls.undead.mvc.Window;
import souls.undead.threads.CounterThread;
import souls.undead.threads.HurtThread;
import souls.undead.utility.Gamelog;

public class NormalHurt implements Hurt {
	public void damageTaken(Character defender, int damage, int element){
		int reduction=0;
		switch(element){
		case 0: // Physical
			reduction = defender.getPdef()+defender.getBonus()[13];
			break;
		case 1: // Magical
			reduction = defender.getMdef()+defender.getBonus()[14];
			break;
		}
		int taken = damage-reduction;
		if (taken<=0)
			taken = 1;		
		if (defender.isInvincible() || defender.getRole().equals("Object")){
			taken = 0;
			if (defender instanceof Pc){
				Gamelog.addText(defender.getName()+" took no damage!");
				(new Thread(new CounterThread(defender))).start();
			}	
		}else{
			if (taken>0){
				(new Thread(new HurtThread(defender))).start();
				defender.setHp(defender.getHp()-taken);
				if (defender instanceof Npc && !defender.getRole().equals("Object")){
					Gamelog.addText(defender.getName()+" lost "+taken+" hp!");
				}
			}
		}
		
		if (defender.isAlive()){
			if (defender instanceof Npc)
				Gamelog.addText(defender.getName()+" has "+defender.getHp()+" hp left!");
		}
		else{
			if (!(defender instanceof Npc)){
				defender.getGame().deathScreen();
			}else{			
				Gamelog.addText(defender.getName()+" has been vanquished");
				
				if (defender.getName().equals("Solaire")){
					defender.getGame().getData().getPlayer().setHp(0);
					defender.getGame().setScreen(Window.VICTORYSCREEN);
				}
				defender.killed();
			}
		}
	}
}
