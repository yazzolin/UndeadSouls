package souls.undead.actions;

import souls.undead.characters.Character;
import souls.undead.mvc.Data;
import souls.undead.threads.DodgeThread;

public class Evasion1 implements Dodge{
	public void avoidDamage(Character c){
//		c.setLastAction("dodge");
		c.setSp(c.getSp()-Data.EVASIONCOST);
		Thread dodge = new Thread(new DodgeThread(c,300) );
		dodge.start();
		c.staminaRegen();
	}
}
