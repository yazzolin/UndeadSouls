package souls.undead.actions;

import java.util.ArrayList;

import souls.undead.characters.Character;
import souls.undead.characters.Pc;
import souls.undead.mvc.Data;
import souls.undead.threads.BurnThread;

public class FireBall extends AttackTemplate implements Skill {

	private static final long serialVersionUID = 1L;
	
	public FireBall(){
		super(3,1,1.5,3);
	}
	
	public void useSkill(Character attacker) {
		int damage = attacker.getPower()+attacker.getBonus()[12];
		ArrayList<Character> targets = atkTarget(attacker,damage,2,true,2);
		for(Character foe : targets){
			if (foe.getStatus()==0)
				((new Thread(new BurnThread(foe,attacker.getPower())))).start();
		}
	}
}
