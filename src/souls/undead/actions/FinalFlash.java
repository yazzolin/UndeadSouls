package souls.undead.actions;

import java.util.ArrayList;

import souls.undead.characters.Character;
import souls.undead.mvc.Data;

public class FinalFlash extends AttackTemplate implements Skill{
	
	public final static String[] sprites = {"hurtbox_beamright.png","hurtbox_beamleft.png","hurtbox_beamup.png","hurtbox_beamdown.png"};
	
	public FinalFlash(){
		super(6,10,0.75,19);
	}
	
	public void useSkill(Character attacker){
		int damage = 3*(attacker.getPower() + attacker.getBonus()[12]);		
		ArrayList<Character> targets = atkTarget(attacker,damage,0,true,3);
		
	}

}
