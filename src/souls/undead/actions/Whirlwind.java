package souls.undead.actions;

import souls.undead.characters.Character;
import souls.undead.mvc.Data;

public class Whirlwind extends AttackTemplate implements Skill {

	private static final long serialVersionUID = 1L;

	public Whirlwind(){
		super(7,5,1.2,1);
	}
	
	public void useSkill(Character attacker) {
		int damage = attacker.getStrength()+attacker.getBonus()[11];	
		damage += 2*attacker.getAgility();
		atkTarget(attacker,damage,1,false,1);
		
	}
}
