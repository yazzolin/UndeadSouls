package souls.undead.threads;

import souls.undead.characters.Character;
import souls.undead.mvc.Game;

public class DodgeThread implements Runnable{

	Character c;
	int duration = 1;
	
	public DodgeThread(Character c, int duration){
		this.c = c;		
		this.duration = duration;
	}
	
	public synchronized void run(){		
		String sprBackup = c.getSprite();
		int dirBackup = c.getDirection();
		c.replaceSprite(c.getDirection(), c.getEvasionSprite());
		c.setInvincible(true);
		c.setInAction(true);
		c.getGame().repaint();
		try{
			Thread.sleep(duration);
		}catch(InterruptedException e){}
		
		c.setInvincible(false);
		c.setInAction(false);
		c.replaceSprite(dirBackup, sprBackup);
		
	}
	
}
