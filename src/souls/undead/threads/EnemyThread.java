package souls.undead.threads;

import souls.undead.characters.Character;
import souls.undead.characters.Enemy;
import souls.undead.characters.Npc;
import souls.undead.characters.Pc;
import souls.undead.mvc.Game;


public class EnemyThread implements Runnable {
	
	Character c;
	Character target;
	
	public EnemyThread(Character c){
		this.c = c;
	}
//	private int distanceTo(Character B){ 
//		return (int) (Math.sqrt(Math.pow((c.getPosition()[0]-B.getPosition()[0]),2)+Math.pow((c.getPosition()[1]-B.getPosition()[1]),2)));
//	}
//	public boolean inAggroRange(){
//		return(distanceTo(c.getGame().getData().getPlayer())<=(((Enemy)(c)).getAggroRange()));
//	}

	public void run(){
		try{
			while(c.isAlive() && ((Npc)c).inAggroRange() && c.getGame().getData().getPlayer().isAlive()){
	//		while(c.isAlive() && c.getGame().getData().getPlayer().isAlive()){
				if((((Enemy)(c)).getPath().size() == 0) || (c.getGame().getData().getPlayer().getPosition()[0]!=((Enemy)(c)).getPathfinding().getFinishNode().getPosition()[0] || c.getGame().getData().getPlayer().getPosition()[1]!=((Enemy)(c)).getPathfinding().getFinishNode().getPosition()[1])){
	//				System.out.println("aggro!");
					((Enemy)c).buildAggroZone(c.getGame().getData().getMapdata());}
	//			else if(!c.getGame().getData().getPlayer().isAlive()){
	////				System.out.println("YOU DIED");
	//				return;}
				else{
					if(((Math.abs(c.getGame().getData().getPlayer().getPosition()[0]-c.getPosition()[0])<=c.getRange()&& c.getGame().getData().getPlayer().getPosition()[1]==c.getPosition()[1]) ||(Math.abs(c.getGame().getData().getPlayer().getPosition()[1]-c.getPosition()[1])<=c.getRange()&&c.getGame().getData().getPlayer().getPosition()[0]==c.getPosition()[0]))){
	//					System.out.println("in range attack! range is "+c.getRange());
						mobAttack();}
					else if(((Enemy)(c)).getPath().get(0).getPosition()[0]==c.getPosition()[0] && ((Enemy)(c)).getPath().get(0).getPosition()[1]==c.getPosition()[1]){
	//					System.out.println("I DID NOT MOVE BITCH");
						((Enemy)(c)).getPath().remove(0);}
					else{
	//					System.out.println("moving");
						mobMovement();}
	//				mobWait();
				}
				c.getGame().refreshView();
			}
		}catch(NullPointerException e){}
	}

	private void mobAttack(){
		if (c.getGame().getData().getPlayer().isAlive()){
			for (int i=0;i<4;i++){
				Character cha = c.getGame().checkAttackCollision(c.getPosition()[0], c.getPosition()[1], i, c.getRange(),false).get(0);
				if (cha!=null && cha instanceof Pc){
					c.setDirection(i);
					break;
				}
			}
			c.attack();
			try {
				Thread.sleep((int)(750/c.getAttackSpeed()));
			} catch (InterruptedException e) {}
		}
	}
	
	private void mobMovement(){
		if (c.getGame().getData().getPlayer().isAlive()){
			if(((Enemy)(c)).getPath().get(0).getPosition()[0]==c.getPosition()[0] +1 && ((Enemy)(c)).getPath().get(0).getPosition()[1]==c.getPosition()[1]){//right move
				c.move(1, 0);
				((Enemy)(c)).getPath().remove(0);
				//System.out.println("move right!");
			}else if(((Enemy)(c)).getPath().get(0).getPosition()[0]==c.getPosition()[0] -1 && ((Enemy)(c)).getPath().get(0).getPosition()[1]==c.getPosition()[1]){//left move
				c.move(-1, 0);
				((Enemy)(c)).getPath().remove(0);
				//System.out.println("move left!");
			}else if(((Enemy)(c)).getPath().get(0).getPosition()[0]==c.getPosition()[0] && ((Enemy)(c)).getPath().get(0).getPosition()[1]==c.getPosition()[1] +1){//down move
				c.move(0, 1);
				((Enemy)(c)).getPath().remove(0);
				//System.out.println("move down!");
			}else if(((Enemy)(c)).getPath().get(0).getPosition()[0]==c.getPosition()[0] && ((Enemy)(c)).getPath().get(0).getPosition()[1]==c.getPosition()[1] -1){//up move
				c.move(0, -1);
				((Enemy)(c)).getPath().remove(0);
				//System.out.println("move up!");
			}
			else{
				//System.out.println("cannot move ^^'");
				//System.out.println("want to go x: " + (((Enemy)(c)).getPath().get(0).getPosition()[0]-c.getPosition()[0]));
				//System.out.println("want to go y: " + (((Enemy)(c)).getPath().get(0).getPosition()[1]-c.getPosition()[1]));
				((Enemy)(c)).getPath().clear();
			}
			try {
				Thread.sleep((int)(750/c.getMovementSpeed()));
			} catch (InterruptedException e) {}
		}
	}
}
