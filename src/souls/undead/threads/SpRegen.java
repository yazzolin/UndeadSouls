package souls.undead.threads;

import souls.undead.characters.Character;
import souls.undead.mvc.Game;

public class SpRegen implements Runnable{
	
	Character player;
	
	public SpRegen(Character player){
		this.player = player;
	}
	
	public synchronized void run(){
		while (player.getSp()!=player.getMsp()){// && !player.isInAction()){
			//System.out.println("regen !");
			//if (!player.isInAction() || !player.getLastAction().equals("attack")){			
				try{
					Thread.sleep(player.getSpRegenDelay());
				}catch(InterruptedException e){}
				player.setSp(player.getSp()+1);
				player.getGame().repaint();
			//}
			
		}
	}

}
