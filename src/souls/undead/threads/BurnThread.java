package souls.undead.threads;

import souls.undead.characters.Character;
import souls.undead.utility.Gamelog;

public class BurnThread implements Runnable{
	
	Character target;
	int spellpower;
	
	public BurnThread(Character target, int spellpower){
		this.target = target;
		this.spellpower = spellpower;
	}
	
	public synchronized void run(){
		if (!target.getRole().equals("Object")){
			Gamelog.addText(target.getName()+" is burnt !");
			target.setStatus(1);
			int n=0;
			while (target.isAlive() && n!=5 && target.getGame().getData().getPlayer().isAlive()){ 
					target.hurt((int)(spellpower/2), 3); // true damage
					try{Thread.sleep(1000);}catch(InterruptedException e){}
					n++;
			}
			target.setStatus(0);
		}
	}

}
