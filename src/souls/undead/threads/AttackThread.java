package souls.undead.threads;

import souls.undead.characters.Character;

public class AttackThread implements Runnable{
	
	Character c;
	int effect = 0;
	double speed = 0;	
	
	public AttackThread(Character c){
		this.c = c;		
	}
	
	public AttackThread(Character c, int effect, double speed){
		this.c = c;
		this.effect = effect;
		this.speed = speed;
	}
	
	public synchronized void run(){
		c.setInAction(true);
		c.setSkillNum(effect);
		String sprBackup = c.getSprite();
		int dirBackup = c.getDirection();
		if (speed==0)
			speed = c.getAttackSpeed();
		try{
			//c.replaceSprite(c.getDirection(), c.getAttackSprite1());
			c.replaceSprite(dirBackup, c.getAttackSprite2());
			Thread.sleep((int)(500/speed));
		}catch(InterruptedException e){}
		c.setSkillNum(0);
		c.setInAction(false);
		c.replaceSprite(dirBackup, sprBackup);
		
	}
}