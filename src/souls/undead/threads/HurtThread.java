package souls.undead.threads;

import souls.undead.characters.Character;

public class HurtThread implements Runnable{
	
	Character c;
	
	public HurtThread(Character c){
		this.c = c;
	}
	
	public synchronized void run(){
		c.setIsHurt(true);
		try{
			Thread.sleep(300);
		}catch(InterruptedException e){}
		c.setIsHurt(false);
	}

}
