package souls.undead.threads;

import souls.undead.characters.Character;
import souls.undead.mvc.Data;
import souls.undead.utility.Gamelog;

public class CounterThread implements Runnable{
	Character c;
	
	public CounterThread(Character c){
		this.c = c;		
	}
	
	public synchronized void run(){
		Gamelog.addText("CounterATK Bonus !");
		//Data.setBASICATKCOST(0);
		//c.setInvincible(true);
		c.setStrength(c.getStrength()+5);
		c.setPower(c.getPower()+5);
		try{
			Thread.sleep(1000);
		}catch(InterruptedException e){}
		c.setStrength(c.getStrength()-5);
		c.setPower(c.getPower()-5);
		//c.setInvincible(false);
		Gamelog.addText("CounterATK Bonus over !");
	}

}
