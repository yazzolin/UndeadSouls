package souls.undead.threads;

import souls.undead.characters.Character;

public class MoveThread implements Runnable {
	
	Character c;
	
	public MoveThread(Character c){
		this.c = c;		
	}
	
	public synchronized void run(){
		double speed = c.getMovementSpeed();
		c.setInAction(true);
		while(c.isInAction()){
			try{
				Thread.sleep((int)(500/speed));
			}catch(InterruptedException e){}
			c.setInAction(false);
		}
	}
}
